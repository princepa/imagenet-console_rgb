import sys
import cv2
import zmq
import os
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append('../../python')
from openpose import *

params = dict()
params["logging_level"] = 3
params["output_resolution"] = "-1x-1"
params["net_resolution"] = "-1x192"
params["model_pose"] = "COCO"
params["alpha_pose"] = 0.6
params["scale_gap"] = 0.3
params["scale_number"] = 1
params["render_threshold"] = 0.05
params["num_gpu_start"] = 0
params["disable_blending"] = False
params["default_model_folder"] = dir_path + "/../../../models/"
openpose = OpenPose(params)



HWM = 4
COPY = False
TIMEOUT = 0  # zmq connection timeout in ms
DELAY = 0.0


def zmq_receiver():
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PULL)
    if not TIMEOUT == 0:
        zmq_socket.RCVTIMEO = TIMEOUT           # timeout time to close connection in ms
    zmq_socket.rcvhwm = HWM
    zmq_socket.bind("tcp://*:5555")
    #zmq_socket.bind("tcp://127.0.0.1:5555")
    # Start your result manager and workers before you start your producers
    return zmq_socket


def zmq_sender():
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.sndhwm = HWM
    # zmq_socket.SNDTIMEO = TIMEOUT
    zmq_socket.bind("tcp://127.0.0.1:5557")
    # Start your result manager and workers before you start your producers
    return zmq_socket


in_img_dir = "/new_openpose_cmu_python/examples/media"
out_img_dir = "/test"
count = 0 

zmq_socket_send = zmq_sender()
zmq_socket_recv = zmq_receiver()


while True:
    t0 = time.time()
    print("Pose waiting for json...")
    md_temp = zmq_socket_recv.recv_json()
    try:
        filename = md_temp['filename']
    except KeyError:
        print('KeyError : filename not found in the received json')
        print("message dropped")
        # to clear the subsequent image in the queue
        msg = zmq_socket_recv.recv(copy=COPY)

        continue


    # print("{} received".format(timer["n_img"]))
    print("{} json received by pose".format(filename))
        
    msg = zmq_socket_recv.recv(copy=COPY)
    print("{} Image received by pose".format(filename))

    try:
        if time.time() - md_temp["time_stamp"] > 10:
            print("image dropped")
            continue
    except KeyError:
        print('KeyError : time_stamp not found in the received json')
        print("message dropped")
        continue
 
    try:
        dtype = md_temp['dtype']
    except KeyError:
        print('KeyError : dtype not found in the received json')
        print("message dropped")
        continue
 
    try:
        shape = md_temp['shape']
    except KeyError:
        print('KeyError : shape not found in the received json')
        print("message dropped")
        continue
 
    count += 1
    # Reconstruct image
    buf = buffer(msg)
    img_matlab = np.frombuffer(buf, dtype=md_temp['dtype'])
 
    # json_data = md['data']
    img = img_matlab.reshape(md_temp['shape'])
 

    t1 = time.time()
    poses,output_image = openpose.forward(img,True)
    #poses = openpose.forward(img,False)
    t2 = time.time()
    
    print("{} images processed in {}s".format(count,t2-t1))    
    print("output shape: {}".format(poses.shape))
    
    out_img_path = os.path.join(out_img_dir,filename)
    cv2.imwrite(out_img_path, output_image)
    print("image saved as {}".format(out_img_path))
