#! /usr/bin/python
# COPYRIGHT FUJITSU LIMITED 2018
# AUTHOR : Shubham Jain
# Date : April, 23, 2018


import os
import sys

import zmq
import numpy as np
import time
import cv2
import json
import ast


usage = "USAGE: python {} port_send port".format(sys.argv[0])

if len(sys.argv)  != 2:
    print(usage)
    sys.exit("Usage Error")


port_send = str(sys.argv[1])

TIMEOUT = 10000  # zmq connection timeout in ms
COUNT = 1     # number of times to send
DELAY = 0.0      # time delay in seconds

context = zmq.Context()
send_socket = context.socket(zmq.PUSH)
send_socket.SNDTIMEO = TIMEOUT
send_socket.connect("tcp://127.0.0.1:"+port_send)


            
# file path 
file_path = "test_image1.jpg" 

for i in range(COUNT):
    if DELAY > 0:
        time.sleep(DELAY)
    # read the image in memory
    img = cv2.imread(file_path)
    filename = file_path

    # required in the json 
    md = dict(
        # shape and dtype is used by api to reconstruct image  
        dtype=str(img.dtype),
        shape=img.shape,
        filename=filename,
        # time_stamp is used by api to calculate message Timeout
        time_stamp=time.time()
    )
    
    # Send json and Image to the api
    # send json before image 
    try:
        send_socket.send_json(md)
        print("{} json sent".format(filename))
        send_socket.send(img)
        print("{} Image sent, #: {}".format(filename,i+1))
    except zmq.error.Again:
        print("Socket Connection Error")
        sys.exit("unable to send Image in {} ms...check receiver".format(TIMEOUT))

print("{} files sent".format(COUNT))

