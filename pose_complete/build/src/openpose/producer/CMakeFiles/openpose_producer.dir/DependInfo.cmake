# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/app/pose_complete/src/openpose/producer/defineTemplates.cpp" "/app/pose_complete/build/src/openpose/producer/CMakeFiles/openpose_producer.dir/defineTemplates.cpp.o"
  "/app/pose_complete/src/openpose/producer/flirReader.cpp" "/app/pose_complete/build/src/openpose/producer/CMakeFiles/openpose_producer.dir/flirReader.cpp.o"
  "/app/pose_complete/src/openpose/producer/imageDirectoryReader.cpp" "/app/pose_complete/build/src/openpose/producer/CMakeFiles/openpose_producer.dir/imageDirectoryReader.cpp.o"
  "/app/pose_complete/src/openpose/producer/ipCameraReader.cpp" "/app/pose_complete/build/src/openpose/producer/CMakeFiles/openpose_producer.dir/ipCameraReader.cpp.o"
  "/app/pose_complete/src/openpose/producer/producer.cpp" "/app/pose_complete/build/src/openpose/producer/CMakeFiles/openpose_producer.dir/producer.cpp.o"
  "/app/pose_complete/src/openpose/producer/spinnakerWrapper.cpp" "/app/pose_complete/build/src/openpose/producer/CMakeFiles/openpose_producer.dir/spinnakerWrapper.cpp.o"
  "/app/pose_complete/src/openpose/producer/videoCaptureReader.cpp" "/app/pose_complete/build/src/openpose/producer/CMakeFiles/openpose_producer.dir/videoCaptureReader.cpp.o"
  "/app/pose_complete/src/openpose/producer/videoReader.cpp" "/app/pose_complete/build/src/openpose/producer/CMakeFiles/openpose_producer.dir/videoReader.cpp.o"
  "/app/pose_complete/src/openpose/producer/webcamReader.cpp" "/app/pose_complete/build/src/openpose/producer/CMakeFiles/openpose_producer.dir/webcamReader.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_CAFFE"
  "USE_CUDA"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "../include"
  "/opt/caffe/include"
  "../PUBLIC"
  "/usr/local/cuda/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/app/pose_complete/build/src/openpose/thread/CMakeFiles/openpose_thread.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/filestream/CMakeFiles/openpose_filestream.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
