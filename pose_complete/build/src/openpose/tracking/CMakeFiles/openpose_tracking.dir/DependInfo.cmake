# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/app/pose_complete/src/openpose/tracking/defineTemplates.cpp" "/app/pose_complete/build/src/openpose/tracking/CMakeFiles/openpose_tracking.dir/defineTemplates.cpp.o"
  "/app/pose_complete/src/openpose/tracking/personIdExtractor.cpp" "/app/pose_complete/build/src/openpose/tracking/CMakeFiles/openpose_tracking.dir/personIdExtractor.cpp.o"
  "/app/pose_complete/src/openpose/tracking/personTracker.cpp" "/app/pose_complete/build/src/openpose/tracking/CMakeFiles/openpose_tracking.dir/personTracker.cpp.o"
  "/app/pose_complete/src/openpose/tracking/pyramidalLK.cpp" "/app/pose_complete/build/src/openpose/tracking/CMakeFiles/openpose_tracking.dir/pyramidalLK.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_CAFFE"
  "USE_CUDA"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "../include"
  "/opt/caffe/include"
  "../PUBLIC"
  "/usr/local/cuda/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
