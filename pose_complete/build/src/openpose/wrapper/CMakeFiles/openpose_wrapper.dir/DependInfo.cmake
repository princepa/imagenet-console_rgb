# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/app/pose_complete/src/openpose/wrapper/defineTemplates.cpp" "/app/pose_complete/build/src/openpose/wrapper/CMakeFiles/openpose_wrapper.dir/defineTemplates.cpp.o"
  "/app/pose_complete/src/openpose/wrapper/wrapperAuxiliary.cpp" "/app/pose_complete/build/src/openpose/wrapper/CMakeFiles/openpose_wrapper.dir/wrapperAuxiliary.cpp.o"
  "/app/pose_complete/src/openpose/wrapper/wrapperStructFace.cpp" "/app/pose_complete/build/src/openpose/wrapper/CMakeFiles/openpose_wrapper.dir/wrapperStructFace.cpp.o"
  "/app/pose_complete/src/openpose/wrapper/wrapperStructHand.cpp" "/app/pose_complete/build/src/openpose/wrapper/CMakeFiles/openpose_wrapper.dir/wrapperStructHand.cpp.o"
  "/app/pose_complete/src/openpose/wrapper/wrapperStructInput.cpp" "/app/pose_complete/build/src/openpose/wrapper/CMakeFiles/openpose_wrapper.dir/wrapperStructInput.cpp.o"
  "/app/pose_complete/src/openpose/wrapper/wrapperStructOutput.cpp" "/app/pose_complete/build/src/openpose/wrapper/CMakeFiles/openpose_wrapper.dir/wrapperStructOutput.cpp.o"
  "/app/pose_complete/src/openpose/wrapper/wrapperStructPose.cpp" "/app/pose_complete/build/src/openpose/wrapper/CMakeFiles/openpose_wrapper.dir/wrapperStructPose.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_CAFFE"
  "USE_CUDA"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "../include"
  "/opt/caffe/include"
  "../PUBLIC"
  "/usr/local/cuda/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/app/pose_complete/build/src/openpose/hand/CMakeFiles/openpose_hand.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/face/CMakeFiles/openpose_face.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/gui/CMakeFiles/openpose_gui.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/utilities/CMakeFiles/openpose_utilities.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/producer/CMakeFiles/openpose_producer.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/thread/CMakeFiles/openpose_thread.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/filestream/CMakeFiles/openpose_filestream.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
