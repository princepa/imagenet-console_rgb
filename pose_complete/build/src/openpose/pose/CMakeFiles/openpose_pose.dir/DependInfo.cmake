# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/app/pose_complete/src/openpose/pose/bodyPartConnectorBase.cpp" "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/bodyPartConnectorBase.cpp.o"
  "/app/pose_complete/src/openpose/pose/bodyPartConnectorCaffe.cpp" "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/bodyPartConnectorCaffe.cpp.o"
  "/app/pose_complete/src/openpose/pose/defineTemplates.cpp" "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/defineTemplates.cpp.o"
  "/app/pose_complete/src/openpose/pose/poseCpuRenderer.cpp" "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/poseCpuRenderer.cpp.o"
  "/app/pose_complete/src/openpose/pose/poseExtractor.cpp" "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/poseExtractor.cpp.o"
  "/app/pose_complete/src/openpose/pose/poseExtractorCaffe.cpp" "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/poseExtractorCaffe.cpp.o"
  "/app/pose_complete/src/openpose/pose/poseExtractorNet.cpp" "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/poseExtractorNet.cpp.o"
  "/app/pose_complete/src/openpose/pose/poseGpuRenderer.cpp" "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/poseGpuRenderer.cpp.o"
  "/app/pose_complete/src/openpose/pose/poseParameters.cpp" "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/poseParameters.cpp.o"
  "/app/pose_complete/src/openpose/pose/poseParametersRender.cpp" "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/poseParametersRender.cpp.o"
  "/app/pose_complete/src/openpose/pose/poseRenderer.cpp" "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/poseRenderer.cpp.o"
  "/app/pose_complete/src/openpose/pose/renderPose.cpp" "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/renderPose.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_CAFFE"
  "USE_CUDA"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "../include"
  "/opt/caffe/include"
  "../PUBLIC"
  "/usr/local/cuda/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
