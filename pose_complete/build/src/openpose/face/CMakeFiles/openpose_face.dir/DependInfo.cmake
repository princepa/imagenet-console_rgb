# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/app/pose_complete/src/openpose/face/defineTemplates.cpp" "/app/pose_complete/build/src/openpose/face/CMakeFiles/openpose_face.dir/defineTemplates.cpp.o"
  "/app/pose_complete/src/openpose/face/faceCpuRenderer.cpp" "/app/pose_complete/build/src/openpose/face/CMakeFiles/openpose_face.dir/faceCpuRenderer.cpp.o"
  "/app/pose_complete/src/openpose/face/faceDetector.cpp" "/app/pose_complete/build/src/openpose/face/CMakeFiles/openpose_face.dir/faceDetector.cpp.o"
  "/app/pose_complete/src/openpose/face/faceDetectorOpenCV.cpp" "/app/pose_complete/build/src/openpose/face/CMakeFiles/openpose_face.dir/faceDetectorOpenCV.cpp.o"
  "/app/pose_complete/src/openpose/face/faceExtractorCaffe.cpp" "/app/pose_complete/build/src/openpose/face/CMakeFiles/openpose_face.dir/faceExtractorCaffe.cpp.o"
  "/app/pose_complete/src/openpose/face/faceExtractorNet.cpp" "/app/pose_complete/build/src/openpose/face/CMakeFiles/openpose_face.dir/faceExtractorNet.cpp.o"
  "/app/pose_complete/src/openpose/face/faceGpuRenderer.cpp" "/app/pose_complete/build/src/openpose/face/CMakeFiles/openpose_face.dir/faceGpuRenderer.cpp.o"
  "/app/pose_complete/src/openpose/face/faceRenderer.cpp" "/app/pose_complete/build/src/openpose/face/CMakeFiles/openpose_face.dir/faceRenderer.cpp.o"
  "/app/pose_complete/src/openpose/face/renderFace.cpp" "/app/pose_complete/build/src/openpose/face/CMakeFiles/openpose_face.dir/renderFace.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_CAFFE"
  "USE_CUDA"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "../include"
  "/opt/caffe/include"
  "../PUBLIC"
  "/usr/local/cuda/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
