# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/app/pose_complete/src/openpose/core/array.cpp" "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/array.cpp.o"
  "/app/pose_complete/src/openpose/core/cvMatToOpInput.cpp" "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/cvMatToOpInput.cpp.o"
  "/app/pose_complete/src/openpose/core/cvMatToOpOutput.cpp" "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/cvMatToOpOutput.cpp.o"
  "/app/pose_complete/src/openpose/core/datum.cpp" "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/datum.cpp.o"
  "/app/pose_complete/src/openpose/core/defineTemplates.cpp" "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/defineTemplates.cpp.o"
  "/app/pose_complete/src/openpose/core/gpuRenderer.cpp" "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/gpuRenderer.cpp.o"
  "/app/pose_complete/src/openpose/core/keepTopNPeople.cpp" "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/keepTopNPeople.cpp.o"
  "/app/pose_complete/src/openpose/core/keypointScaler.cpp" "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/keypointScaler.cpp.o"
  "/app/pose_complete/src/openpose/core/opOutputToCvMat.cpp" "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/opOutputToCvMat.cpp.o"
  "/app/pose_complete/src/openpose/core/point.cpp" "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/point.cpp.o"
  "/app/pose_complete/src/openpose/core/rectangle.cpp" "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/rectangle.cpp.o"
  "/app/pose_complete/src/openpose/core/renderer.cpp" "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/renderer.cpp.o"
  "/app/pose_complete/src/openpose/core/scaleAndSizeExtractor.cpp" "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/scaleAndSizeExtractor.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_CAFFE"
  "USE_CUDA"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "../include"
  "/opt/caffe/include"
  "../PUBLIC"
  "/usr/local/cuda/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
