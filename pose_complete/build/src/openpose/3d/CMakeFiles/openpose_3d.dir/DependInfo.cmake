# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/app/pose_complete/src/openpose/3d/cameraParameterReader.cpp" "/app/pose_complete/build/src/openpose/3d/CMakeFiles/openpose_3d.dir/cameraParameterReader.cpp.o"
  "/app/pose_complete/src/openpose/3d/defineTemplates.cpp" "/app/pose_complete/build/src/openpose/3d/CMakeFiles/openpose_3d.dir/defineTemplates.cpp.o"
  "/app/pose_complete/src/openpose/3d/poseTriangulation.cpp" "/app/pose_complete/build/src/openpose/3d/CMakeFiles/openpose_3d.dir/poseTriangulation.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_CAFFE"
  "USE_CUDA"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "../include"
  "/opt/caffe/include"
  "../PUBLIC"
  "/usr/local/cuda/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
