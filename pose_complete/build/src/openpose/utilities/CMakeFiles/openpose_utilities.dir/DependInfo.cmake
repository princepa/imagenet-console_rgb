# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/app/pose_complete/src/openpose/utilities/errorAndLog.cpp" "/app/pose_complete/build/src/openpose/utilities/CMakeFiles/openpose_utilities.dir/errorAndLog.cpp.o"
  "/app/pose_complete/src/openpose/utilities/fileSystem.cpp" "/app/pose_complete/build/src/openpose/utilities/CMakeFiles/openpose_utilities.dir/fileSystem.cpp.o"
  "/app/pose_complete/src/openpose/utilities/flagsToOpenPose.cpp" "/app/pose_complete/build/src/openpose/utilities/CMakeFiles/openpose_utilities.dir/flagsToOpenPose.cpp.o"
  "/app/pose_complete/src/openpose/utilities/keypoint.cpp" "/app/pose_complete/build/src/openpose/utilities/CMakeFiles/openpose_utilities.dir/keypoint.cpp.o"
  "/app/pose_complete/src/openpose/utilities/openCv.cpp" "/app/pose_complete/build/src/openpose/utilities/CMakeFiles/openpose_utilities.dir/openCv.cpp.o"
  "/app/pose_complete/src/openpose/utilities/profiler.cpp" "/app/pose_complete/build/src/openpose/utilities/CMakeFiles/openpose_utilities.dir/profiler.cpp.o"
  "/app/pose_complete/src/openpose/utilities/string.cpp" "/app/pose_complete/build/src/openpose/utilities/CMakeFiles/openpose_utilities.dir/string.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_CAFFE"
  "USE_CUDA"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "../include"
  "/opt/caffe/include"
  "../PUBLIC"
  "/usr/local/cuda/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/app/pose_complete/build/src/openpose/producer/CMakeFiles/openpose_producer.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/filestream/CMakeFiles/openpose_filestream.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/thread/CMakeFiles/openpose_thread.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
