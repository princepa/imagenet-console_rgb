# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/app/pose_complete/src/openpose/filestream/cocoJsonSaver.cpp" "/app/pose_complete/build/src/openpose/filestream/CMakeFiles/openpose_filestream.dir/cocoJsonSaver.cpp.o"
  "/app/pose_complete/src/openpose/filestream/defineTemplates.cpp" "/app/pose_complete/build/src/openpose/filestream/CMakeFiles/openpose_filestream.dir/defineTemplates.cpp.o"
  "/app/pose_complete/src/openpose/filestream/fileSaver.cpp" "/app/pose_complete/build/src/openpose/filestream/CMakeFiles/openpose_filestream.dir/fileSaver.cpp.o"
  "/app/pose_complete/src/openpose/filestream/fileStream.cpp" "/app/pose_complete/build/src/openpose/filestream/CMakeFiles/openpose_filestream.dir/fileStream.cpp.o"
  "/app/pose_complete/src/openpose/filestream/heatMapSaver.cpp" "/app/pose_complete/build/src/openpose/filestream/CMakeFiles/openpose_filestream.dir/heatMapSaver.cpp.o"
  "/app/pose_complete/src/openpose/filestream/imageSaver.cpp" "/app/pose_complete/build/src/openpose/filestream/CMakeFiles/openpose_filestream.dir/imageSaver.cpp.o"
  "/app/pose_complete/src/openpose/filestream/jsonOfstream.cpp" "/app/pose_complete/build/src/openpose/filestream/CMakeFiles/openpose_filestream.dir/jsonOfstream.cpp.o"
  "/app/pose_complete/src/openpose/filestream/keypointSaver.cpp" "/app/pose_complete/build/src/openpose/filestream/CMakeFiles/openpose_filestream.dir/keypointSaver.cpp.o"
  "/app/pose_complete/src/openpose/filestream/peopleJsonSaver.cpp" "/app/pose_complete/build/src/openpose/filestream/CMakeFiles/openpose_filestream.dir/peopleJsonSaver.cpp.o"
  "/app/pose_complete/src/openpose/filestream/videoSaver.cpp" "/app/pose_complete/build/src/openpose/filestream/CMakeFiles/openpose_filestream.dir/videoSaver.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_CAFFE"
  "USE_CUDA"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "../include"
  "/opt/caffe/include"
  "../PUBLIC"
  "/usr/local/cuda/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
