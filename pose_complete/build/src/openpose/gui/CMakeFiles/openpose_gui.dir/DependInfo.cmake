# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/app/pose_complete/src/openpose/gui/defineTemplates.cpp" "/app/pose_complete/build/src/openpose/gui/CMakeFiles/openpose_gui.dir/defineTemplates.cpp.o"
  "/app/pose_complete/src/openpose/gui/frameDisplayer.cpp" "/app/pose_complete/build/src/openpose/gui/CMakeFiles/openpose_gui.dir/frameDisplayer.cpp.o"
  "/app/pose_complete/src/openpose/gui/gui.cpp" "/app/pose_complete/build/src/openpose/gui/CMakeFiles/openpose_gui.dir/gui.cpp.o"
  "/app/pose_complete/src/openpose/gui/gui3D.cpp" "/app/pose_complete/build/src/openpose/gui/CMakeFiles/openpose_gui.dir/gui3D.cpp.o"
  "/app/pose_complete/src/openpose/gui/guiInfoAdder.cpp" "/app/pose_complete/build/src/openpose/gui/CMakeFiles/openpose_gui.dir/guiInfoAdder.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_CAFFE"
  "USE_CUDA"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/opencv"
  "../include"
  "/opt/caffe/include"
  "../PUBLIC"
  "/usr/local/cuda/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/app/pose_complete/build/src/openpose/pose/CMakeFiles/openpose_pose.dir/DependInfo.cmake"
  "/app/pose_complete/build/src/openpose/core/CMakeFiles/openpose_core.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
