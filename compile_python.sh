#!/bin/bash
# This script should be copied to the intermediate docker to convert python source files to executables within this docker image

# Syntax to run the script is shown below:
# ./compile_python.sh 

echo "compiling python source files ... "

echo "removing previously compiled files..."

rm -rf build
mkdir build

nuitka --module --no-pyi-file --remove-output --output-dir=build source/config_parser.py
nuitka --module --no-pyi-file --remove-output --output-dir=build source/uvdetect.py
nuitka --module --no-pyi-file --remove-output --output-dir=build source/json_utils.py
nuitka --module --no-pyi-file --remove-output --output-dir=build source/_init_paths.py
nuitka --module --no-pyi-file --remove-output --output-dir=build source/get_head_box.py
nuitka --module --no-pyi-file --remove-output --output-dir=build source/pose.py

nuitka --no-pyi-file --remove-output --output-dir=build source/demo.py
nuitka --no-pyi-file --remove-output --output-dir=build source/extract_pose.py
nuitka --no-pyi-file --remove-output --output-dir=build source/head_face_classifier.py
nuitka --no-pyi-file --remove-output --output-dir=build source/image_input.py
nuitka --no-pyi-file --remove-output --output-dir=build source/person_classifiers_accumulator.py 
nuitka --no-pyi-file --remove-output --output-dir=build source/sink.py
nuitka --no-pyi-file --remove-output --output-dir=build source/get_face_box.py
nuitka --no-pyi-file --remove-output --output-dir=build source/get_head_dir.py

echo "waiting to finish compilation..."
wait 

echo "done creating executables"
