#gpg -c "models/person/person.cfg"
#gpg -c "models/person/person.data"
#gpg -c "models/person/person.weights"

#endswith=".cfg,.data,.weights,.prototxt,.caffemodel"

for filename in models/*/* 
    do
        if [[ $( basename $filename ) == *.gpg ]] ; then
            echo $(basename "$filename")
            #gpg -c --passphrase uncanny $filename
    fi
done


# if [[ $( basename $filename ) == *.cfg || $( basename $filename ) == *.data || $( basename $filename ) == *.weights || $( basename $filename ) == *.caffemodel || $( basename $filename ) == *.prototxt ]] ; then
