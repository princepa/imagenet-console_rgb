package_folder=$1_$2_package
image_build_name=$1
image_tag=$2


echo "creating package in path: $package_folder"

echo "building image: $1:$2"

mkdir ../$package_folder
echo "compiling python..."
sh compile_python.sh

echo "copying required files to package folder..."
cp -r build 3rdParty config start.sh models Dockerfile ../$package_folder/

cd ../$package_folder
docker build -t $1:$2 .

cd ~/shubham/fujitsu

echo "saving $1:$2 docker image as $1_$2 tar file ..."
docker save $1:$2 > $1_$2.tar
