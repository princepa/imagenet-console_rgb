#!/bin/sh

# This script is to be used by the temporary docker container to copy the executables that were generated in it.

cp /app/dist/* /app/temp/
