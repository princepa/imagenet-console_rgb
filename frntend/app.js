var express = require('express');
var app = express();
var fs = require("fs");
var multer = require('multer');
const junk = require('junk');
var uniqid = require('uniqid');
var exec = require('child_process').exec;
//var sleep = require('sleep');

var upload = multer({
  dest: '/tmp'
})

app.use(express.static(__dirname + "/"));
app.use(express.static("../"));
//app.use(express.static("/home/uncannay/workspace/projects/fujitsu-zmq-delete-models/"));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.get('/gallery', function(req, res, next) {

  res.sendFile(__dirname + '/resources/html/gallery.html');

});

app.post('/upload', upload.single("file"), function(req, res, next) {

  var uid = uniqid();

  var file = __dirname + "/data/images/uploads/" + uid + ".png";

  fs.readFile(req.file.path, function(err, data) {

    fs.writeFile(file, data, function(err) {

      if (err) {
        console.error(err);
        res.end('Sorry, the file could not be uploaded');
        return;
      } else {

        var args = "-X POST " + 'http://localhost:5000/upload' + " -F file=@/" + __dirname + "/data/images/uploads/" + uid + ".png";

        console.log("curl " + args);
        exec('curl ' + args, function (error, stdout, stderr) {
          console.log('stderr: ' + stderr);
          res.send(stdout);
          if (error !== null) {
            console.log('exec error: ' + error);
          }
        });
      }

    });

  });
  /*fs.writeFile(file2, data, function(err) {

  if (err) {
  console.error(err);
  res.end('Sorry, the file could not be uploaded');
  return;
}
});*/

/*

var i = 0;
var status = true;
while (!fs.existsSync(__dirname + "/data/jsons/uploads/" + uid + '.json')) {
i++;

processjsons();
console.log("Waiting for file...");
sleep.msleep(3000);

if ( i == 5)
{
status = false;
console.log("Invalid file or not able to get data from the file provided");
break;
}
}

if (status)
{
console.log("Showing result");
var obj = JSON.parse(fs.readFileSync(__dirname + "/public/jsons/uploads/" + uid + '.json', 'utf8'));
res.end( JSON.stringify( obj ) );

}
else {
res.end("Failed to send reponse");

}
}
});
});*/

});

app.get('/', function(req, res, next) {

  res.sendFile(__dirname + '/resources/html/file_upload.html');

});

app.get('/functions/getUploadedImages', function(req, res, next) {

  //var files = fs.readdirSync(__dirname + "/data/images/uploads/");
  var files = fs.readdirSync(__dirname + "/../uploads/");

  res.send(files.filter(junk.not));

});

app.get('/functions/getUploadedJsons', function(req, res, next) {

  //var files = fs.readdirSync(__dirname + "/data/jsons/uploads/");
  var files = fs.readdirSync(__dirname + "/../results/");

  res.send(files.filter(junk.not));

});

var server = app.listen(8085, function() {
  var host = server.address().address
  var port = server.address().port

  console.log("uv server listening")
})
