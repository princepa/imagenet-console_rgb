$(document).ready(function(){
  $( "#prev_image" ).click(function(){
    prev();
  });
  $( "#next_image" ).click(function(){
    next();
  });
});

var images = [], jsons = [];

$.get("/functions/getUploadedImages", function(data, status){
  images = data;
  var image = document.getElementById('slideshow_image');
  document.getElementById('slideshow_image').src = "/../uploads/" + images[0];
  image.onload = function (){
    loadImage();
  }
});

$.get("/functions/getUploadedJsons", function(data, status){
  jsons = data;
});

function prev()
{
  $( '#slideshow_image' ).fadeOut(75,function()
  {
    var prev_val = document.getElementById( "img_no" ).value;
    var prev_val = Number(prev_val) - 1;
    if(prev_val <= -1)
    {
      prev_val = images.length - 1;
    }
    $( '#slideshow_image' ).attr( 'src' , '../uploads/'+images[prev_val]);
    document.getElementById( "img_no" ).value = prev_val;
  });
  $( '#slideshow_image' ).fadeIn(250);

  $("img").one("load", function() {
    loadImage();
  })
}

function next()
{
  $( '#slideshow_image' ).fadeOut(75,function()
  {
    var next_val = document.getElementById( "img_no" ).value;
    var next_val = Number(next_val)+1;
    if(next_val >= images.length)
    {
      next_val = 0;
    }
    $( '#slideshow_image' ).attr( 'src' , '../uploads/'+images[next_val]);
    document.getElementById( "img_no" ).value = next_val;
  });
  $( '#slideshow_image' ).fadeIn(250);

  $("img").one("load", function() {
    loadImage();
  })
}

function loadImage()
{
  var image = document.getElementById('slideshow_image');
  var current_image = document.getElementById('slideshow_image').src;

  if (current_image == "")
  {
    $.get("/functions/getUploadedImages", function(data, status){
      images = data;

      document.getElementById('slideshow_image').src = "/../uploads/" + images[0];
      current_image = document.getElementById('slideshow_image').src;
      var file = current_image.split('/').pop();
      var filename=file.split('.').shift();
      processjson(filename);
    });
  }
  else {
    var file = current_image.split('/').pop();
    var filename=file.split('.').shift();
    processjson(filename);
  }
}

function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

function processjson(filename) {

  var json;

  $.get("/../results/" + filename + ".json", function(data, status){

    json = data;

    document.getElementById('filename').value = "Filename: " + json.filename;
    document.getElementById('tpeople').value = "People: " + json.people.length;
    document.getElementById('tfaces').value = "Faces: " + json.faces.length;
    document.getElementById('theads').value = "Heads: " + json.heads.length;
    document.getElementById('tobjects').value = "Objects: " + json.objects.length;

    document.getElementById("download_json").onclick = function() {
      download(filename + ".json", JSON.stringify(json));
    };

    var bottompane = document.getElementById('bottompane');
    while (bottompane.hasChildNodes()) {
      bottompane.removeChild(bottompane.lastChild);
    }

    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    var img = document.getElementById("slideshow_image");

    canvas.width = img.width;
    canvas.height = img.height;

    ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

    for(var i=0; i< json.faces.length; i++)
    {

      var canvas = document.getElementById('myCanvas');
      var context = canvas.getContext('2d');

      var color = document.getElementById('facecolor').value;

      context.beginPath();
      if (color != "none")
      context.rect(json.faces[i].coordinates.xmin, json.faces[i].coordinates.ymin, json.faces[i].coordinates.width,
      json.faces[i].coordinates.height);
      context.lineWidth = 2;
      context.strokeStyle = color;
      context.stroke();

      var bottomdiv = document.createElement('div');
      bottomdiv.setAttribute("class","child");

      var mShouldShowDiv = verifyFilter(json.faces[i].properties);

      var face_area = json.faces[i].coordinates.width*json.faces[i].coordinates.width;

      if (!document.getElementById("LessSize").checked && face_area < 10000)
      mShouldShowDiv = false;

      if (!document.getElementById("GreatSize").checked && face_area > 10000)
      mShouldShowDiv = false;

      if (mShouldShowDiv)
      bottompane.appendChild(bottomdiv);

      var cropcanv = document.createElement('canvas');
      var fullimage = document.getElementById('slideshow_image');

      if (cropcanv.getContext) {

        var context = cropcanv.getContext("2d");

        cropcanv.width = 100;
        cropcanv.height = 100;

        context.drawImage(fullimage, json.faces[i].coordinates.xmin, json.faces[i].coordinates.ymin,
          json.faces[i].coordinates.width,
          json.faces[i].coordinates.height,
          0,0,cropcanv.width, cropcanv.height);

          var leftcont = document.createElement('div');
          leftcont.setAttribute("class","newchild")
          leftcont.appendChild(cropcanv);

          bottomdiv.appendChild(leftcont);
        }

        var rightcont = document.createElement('div');
        rightcont.setAttribute("class","newchild")
        rightcont.appendChild(document.createTextNode(
          ' Gender: ' + findGreatest(json.faces[i].properties.gender, 0) + '(' + findGreatest(json.faces[i].properties.gender, 1) + ')' + '\n'
          +' Age: ' + findGreatest(json.faces[i].properties.age, 0)+ '(' + findGreatest(json.faces[i].properties.age, 1) + ')' + '\n'
          +' Race: ' + findGreatest(json.faces[i].properties.race, 0) + '(' + findGreatest(json.faces[i].properties.race, 1) + ')' + '\n'
          +' EyeAcc: ' + findGreatest(json.faces[i].properties.eye_acc, 0) + '(' + findGreatest(json.faces[i].properties.eye_acc, 1) + ')' + '\n'
          +' Size: ' + json.faces[i].coordinates.width
          + '*' + json.faces[i].coordinates.height + '\n'));

          bottomdiv.style = "white-space: pre;";
          bottomdiv.appendChild(rightcont);
        }

        for (var i = 0;i < json.people.length; i++) {
          var canvas = document.getElementById('myCanvas');
          var context = canvas.getContext('2d');

          var color = document.getElementById('peoplecolor').value;

          context.beginPath();
          if (color != "none")
          context.rect(json.people[i].coordinates.xmin, json.people[i].coordinates.ymin, json.people[i].coordinates.width,
          json.people[i].coordinates.height);
          context.lineWidth = 2;
          context.strokeStyle = color;
          context.stroke();

          var bottomdiv = document.createElement('div');
          bottomdiv.setAttribute("class","child");

          //var mShowObjects = showObjects();

          var mShouldShowDiv = verifyFilterPeople(json.people[i].properties);

          if (mShouldShowDiv)
          bottompane.appendChild(bottomdiv);

          var cropcanv = document.createElement('canvas');
          var fullimage = document.getElementById('slideshow_image');

          if (cropcanv.getContext) {

            var context = cropcanv.getContext("2d");

            cropcanv.width = 100;
            cropcanv.height = 100;

            context.drawImage(fullimage, json.people[i].coordinates.xmin, json.people[i].coordinates.ymin,
              json.people[i].coordinates.width,
              json.people[i].coordinates.height,
              0,0,cropcanv.width, cropcanv.height);

              var leftcont = document.createElement('div');
              leftcont.setAttribute("class","newchild")
              leftcont.appendChild(cropcanv);

              bottomdiv.appendChild(leftcont);
            }

            var rightcont = document.createElement('div');
            rightcont.setAttribute("class","newchild")

            rightcont.appendChild(document.createTextNode(
              ' Upper Body: ' + findGreatest(json.people[i].properties.upper_body_clothing, 0) + '(' + findGreatest(json.people[i].properties.upper_body_clothing, 1) + ')' + '\n'
              +' Lower Body: ' + findGreatest(json.people[i].properties.lower_body_clothing, 0)+ '(' + findGreatest(json.people[i].properties.lower_body_clothing, 1) + ')' + '\n'
              +' Person Gender: ' + findGreatest(json.people[i].properties.person_gender, 0)+ '(' + findGreatest(json.people[i].properties.person_gender, 1) + ')' + '\n'
              +' Person Age: ' + findGreatest(json.people[i].properties.person_age, 0)+ '(' + findGreatest(json.people[i].properties.person_age, 1) + ')' + '\n'
              +' Hand Position: ' + findGreatest(json.people[i].properties.hand_position, 0) + '(' + findGreatest(json.people[i].properties.hand_position, 1) + ')'));

            bottomdiv.style = "white-space: pre;";
            bottomdiv.appendChild(rightcont);
        }

        for (var i = 0;i < json.heads.length; i++) {

        var canvas = document.getElementById('myCanvas');
        var context = canvas.getContext('2d');

        var color = document.getElementById('headscolor').value;

        context.beginPath();
        if (color != "none")
        context.rect(json.heads[i].coordinates.xmin, json.heads[i].coordinates.ymin, json.heads[i].coordinates.width,
        json.heads[i].coordinates.height);
        context.lineWidth = 2;
        context.strokeStyle = color;
        context.stroke();

        var bottomdiv = document.createElement('div');
        bottomdiv.setAttribute("class","child");

        var mShouldShowDiv = verifyFilterHeads(json.heads[i].properties);

        if (mShouldShowDiv)
        bottompane.appendChild(bottomdiv);

        var cropcanv = document.createElement('canvas');
        var fullimage = document.getElementById('slideshow_image');

        if (cropcanv.getContext) {

          var context = cropcanv.getContext("2d");

          cropcanv.width = 100;
          cropcanv.height = 100;

          context.drawImage(fullimage, json.heads[i].coordinates.xmin, json.heads[i].coordinates.ymin,
            json.heads[i].coordinates.width,
            json.heads[i].coordinates.height,
            0,0,cropcanv.width, cropcanv.height);

            var leftcont = document.createElement('div');
            leftcont.setAttribute("class","newchild")
            leftcont.appendChild(cropcanv);

            bottomdiv.appendChild(leftcont);
          }

          var rightcont = document.createElement('div');
          rightcont.setAttribute("class","newchild")

          rightcont.appendChild(document.createTextNode(
            ' Head Direction: ' + findGreatest(json.heads[i].properties.headdirection, 0) + '(' + findGreatest(json.heads[i].properties.headdirection, 1) + ')' +'\n'
            +' Face Mask: ' + findGreatest(json.heads[i].properties.face_mask, 0) + '(' + findGreatest(json.heads[i].properties.face_mask, 1) + ')' + '\n'
            + ' Head Gear: ' + findGreatest(json.heads[i].properties.head_gear, 0) + '(' + findGreatest(json.heads[i].properties.head_gear, 1) + ')'));


          bottomdiv.style = "white-space: pre;";
          bottomdiv.appendChild(rightcont);

	     }

        for (var i = 0;i < json.objects.length; i++) {
          var canvas = document.getElementById('myCanvas');
          var context = canvas.getContext('2d');

          var color = document.getElementById('objectscolor').value;

          context.beginPath();
          if (color != "none")
          context.rect(json.objects[i].coordinates.xmin, json.objects[i].coordinates.ymin, json.objects[i].coordinates.width,
          json.objects[i].coordinates.height);
          context.lineWidth = 2;
          context.strokeStyle = color;
          context.stroke();

          bottomdiv.setAttribute("class","child")

          var mShowObjects = showObjects();

          var bottomdiv = document.createElement('div');

          if (mShowObjects)
          bottompane.appendChild(bottomdiv);

          var cropcanv = document.createElement('canvas');
          var fullimage = document.getElementById('slideshow_image');

          if (cropcanv.getContext) {

            var context = cropcanv.getContext("2d");

            cropcanv.width = 100;
            cropcanv.height = 100;

            context.drawImage(fullimage, json.objects[i].coordinates.xmin, json.objects[i].coordinates.ymin,
              json.objects[i].coordinates.width,
              json.objects[i].coordinates.height,
              0,0,cropcanv.width, cropcanv.height);

              var leftcont = document.createElement('div');
              leftcont.setAttribute("class","newchild")
              leftcont.appendChild(cropcanv);

              bottomdiv.appendChild(leftcont);
            }

            var rightcont = document.createElement('div');
            rightcont.setAttribute("class","newchild")
            rightcont.appendChild(document.createTextNode(json.objects[i].name));

            bottomdiv.style = "white-space: pre;";
            bottomdiv.appendChild(rightcont);
          }

          document.getElementById('tmales').value = "Males: " + countObjects(json.faces, "gender",  "Male");
          document.getElementById('tfemales').value = "Females: " + countObjects(json.faces, "gender",  "Female");;

          document.getElementById('tzerotonine').value = "0-9: " + countObjects(json.faces, "age",  "0-9");
          document.getElementById('ttentonineteen').value = "10-19: " + countObjects(json.faces, "age",  "10-19");
          document.getElementById('ttwentytofifty').value = "20-50: " + countObjects(json.faces, "age",  "20-50");
          document.getElementById('tfiftyplus').value = "50+: " + countObjects(json.faces, "age",  "50+");

          document.getElementById('tspectacles').value = "Spectacles: " + countObjects(json.faces, "eye_acc",  "Specs");
          document.getElementById('tsunglasses').value = "Sunglasses: " + countObjects(json.faces, "eye_acc",  "Sunglasses");
          document.getElementById('tnoeyeaccessory').value = "None: " + countObjects(json.faces, "eye_acc",  "None");

          document.getElementById('tmasks').value = "Mask: " + countObjects(json.heads, "face_mask",  "Mask");
          document.getElementById('tnomasks').value = "No Mask: " + countObjects(json.heads, "face_mask",  "No-Mask");

          document.getElementById('tsouthasians').value = "South Asians: " + countObjects(json.faces, "race",  "South-Asian");
          document.getElementById('teastasians').value = "East Asians: " + countObjects(json.faces, "race",  "East-Asian");
          document.getElementById('tcaucasian').value = "Caucasians: " + countObjects(json.faces, "race",  "Caucasian");
          document.getElementById('tafricans').value = "Africans: " + countObjects(json.faces, "race", "African");

          document.getElementById('tnormalupper').value = "Normal Upper: " + countObjects(json.people, "upper_body_clothing",  "Normal");
          document.getElementById('tlargejacket').value = "Large Jacket: " + countObjects(json.people, "upper_body_clothing",  "Large-Jacket");
          document.getElementById('tworkervest').value = "Worker Vest: " + countObjects(json.people, "upper_body_clothing",  "Worker-Vest");
          document.getElementById('tsuit').value = "Suit: " + countObjects(json.people, "upper_body_clothing", "Suit");
          document.getElementById('tstudentuniform').value = "Student Uniform: " + countObjects(json.people, "upper_body_clothing", "Student-Uniform");

          document.getElementById('tnormallower').value = "Normal Lower: " + countObjects(json.people, "lower_body_clothing",  "Normal");
          document.getElementById('tbaggypants').value = "Baggy Pants: " + countObjects(json.people, "lower_body_clothing",  "Baggy-Pants");
          document.getElementById('tskirt').value = "Skirt: " + countObjects(json.people, "lower_body_clothing",  "Skirt");

          document.getElementById('tpmale').value = "Person Gender Male: " + countObjects(json.people, "person_gender",  "Male");
          document.getElementById('tpfemale').value = "Person Gender Female: " + countObjects(json.people, "person_gender",  "Female");

          document.getElementById('tpzerotonine').value = "Person Age 0-9: " + countObjects(json.people, "person_age",  "0-9");
          document.getElementById('tptentonineteen').value = "Person Age 10-19: " + countObjects(json.people, "person_age",  "10-19");
          document.getElementById('tptwentytofifty').value = "Person Age 20-50: " + countObjects(json.people, "person_age",  "20-50");
          document.getElementById('tpfiftyplus').value = "Person Age 50+: " + countObjects(json.people, "person_age",  "50+");

          document.getElementById('treachingproduct').value = "Reaching-Product: " + countObjects(json.people, "hand_position",  "Reaching-Product");
          document.getElementById('ttouchingface').value = "Touching Face: " + countObjects(json.people, "hand_position",  "Touching-Face");
          document.getElementById('tontrolley').value = "On Trolley: " + countObjects(json.people, "hand_position",  "On-Trolley");
          document.getElementById('tbyside').value = "By Side: " + countObjects(json.people, "hand_position", "By-Side");

          document.getElementById('tcap').value = "Cap/Hat/Cycle-Helmet: " + countObjects(json.heads, "head_gear",  "Cap/Hat/Cycle-Helmet");
          document.getElementById('tcyclehelmet').value = "Full Helmet: " + countObjects(json.heads, "head_gear",  "Full-Helmet");
          document.getElementById('theadnone').value = "None: " + countObjects(json.heads, "head_gear",  "None");

          document.getElementById('tleft').value = "Left: " + countObjects(json.heads, "headdirection", "Left");
          document.getElementById('tstraight').value = "Straight: " + countObjects(json.heads, "headdirection", "Straight");
          document.getElementById('tright').value = "Right: " + countObjects(json.heads, "headdirection", "Right");
          document.getElementById('tbottom').value = "Bottom: " + countObjects(json.heads, "headdirection", "Bottom");
          document.getElementById('tback').value = "Back: " + countObjects(json.heads, "headdirection", "Back");
        });
      }

      function verifyFilter(obj)
      {
        var shouldshow = true;

        if (obj.gender != null && !document.getElementById(findGreatest(obj.gender, 0)).checked)
        shouldshow = false;

        if (obj.age != null && !document.getElementById(findGreatest(obj.age, 0)).checked)
        shouldshow = false;

        if (obj.race != null && !document.getElementById(findGreatest(obj.race, 0)).checked)
        shouldshow = false;

        if (obj.eye_acc != null && !document.getElementById('Yes').checked && obj.eye_acc != "none")
        shouldshow = false;

        if (obj.eye_acc != null && !document.getElementById('No').checked && obj.eye_acc == "none")
        shouldshow = false;

        return shouldshow;

      }

      function verifyFilterPeople(obj)
      {
        var shouldshow = true;

        if (document.getElementById(findGreatest(obj.upper_body_clothing, 0)) && obj.upper_body_clothing != null && !document.getElementById(findGreatest(obj.upper_body_clothing, 0)).checked)
        shouldshow = false;

        if (obj.hand_position != null && !document.getElementById(findGreatest(obj.hand_position, 0)).checked)
        shouldshow = false;

        if (document.getElementById(findGreatest(obj.lower_body_clothing, 0)) && obj.lower_body_clothing != null && !document.getElementById(findGreatest(obj.lower_body_clothing, 0)).checked)
        shouldshow = false;

        if (obj.person_age != null && !document.getElementById("t" + findGreatest(obj.person_age, 0)).checked)
        shouldshow = false;

        if (obj.person_gender != null && !document.getElementById("p" + findGreatest(obj.person_gender, 0)).checked)
        shouldshow = false;

        return shouldshow;

      }

      function verifyFilterHeads(obj)
      {
        var shouldshow = true;

        if (obj.head_gear != null && !document.getElementById(findGreatest(obj.head_gear, 0)).checked)
        shouldshow = false;

        if (obj.headdirection != null && !document.getElementById(findGreatest(obj.headdirection, 0)).checked)
        shouldshow = false;

        if (obj.face_mask != null && !document.getElementById(findGreatest(obj.face_mask, 0)).checked)
        shouldshow = false;

        return shouldshow;

      }

      function getCheckedCheckboxesFor(checkboxName) {
        var checkboxes = document.querySelectorAll('input[name="' + checkboxName + '"]:checked'), values = [];
        Array.prototype.forEach.call(checkboxes, function(el) {
          values.push(el.value);
        });
        return values;
      }

      function showObjects()
      {
        var mShowObjects = getCheckedCheckboxesFor("showobjects");
        if (mShowObjects.indexOf("Show Objects") > -1) {
          return true;
        } else {
          return false;
        }
      }

      function handleChange()
      {
        loadImage();
      }

      function findGreatest(obj, option)
      {
        var name = "";
        var confidence = 0;

        if (obj == null)
        {
          name = "Unknown";
          confidence = 0;
        }
        else {
          for (var i = 0; i < obj.length; i++)
          {
            if (confidence < obj[i].confidence)
            {
              confidence = obj[i].confidence;
              name = obj[i].name;
            }
          }
        }

        if (option == 0)
        {
          return name;
        }
        else {
            confidence = Math.round(confidence*100);
          return confidence;
        }
      }

      function countObjects(obj, objecttype, value) {
        var count = 0;

        if (objecttype == "race")
        {
          for (var i = 0; i < obj.length; i++)
          {
            if (findGreatest(obj[i].properties.race, 0) == value)
            {
              count++;
            }
          }
        }

        if (objecttype == "gender")
        {
          for (var i = 0; i < obj.length; i++)
          {
            if (findGreatest(obj[i].properties.gender, 0) == value)
            {
              count++;
            }
          }
        }

        if (objecttype == "age")
        {
          for (var i = 0; i < obj.length; i++)
          {
            if (findGreatest(obj[i].properties.age, 0) == value)
            {
              count++;
            }
          }
        }

        if (objecttype == "headdirection")
        {
          for (var i = 0; i < obj.length; i++)
          {
            if (findGreatest(obj[i].properties.headdirection, 0) == value)
            {
              count++;
            }
          }
        }

        if (objecttype == "eye_acc")
        {
          for (var i = 0; i < obj.length; i++)
          {
            if (findGreatest(obj[i].properties.eye_acc, 0) == value)
            {
              count++;
            }
          }
        }

        if (objecttype == "face_mask")
        {
          for (var i = 0; i < obj.length; i++)
          {
            if (findGreatest(obj[i].properties.face_mask, 0) == value)
            {
              count++;
            }
          }
        }

        if (objecttype == "upper_body_clothing")
        {
          for (var i = 0; i < obj.length; i++)
          {
            if (findGreatest(obj[i].properties.upper_body_clothing, 0) == value)
            {
              count++;
            }
          }
        }

        if (objecttype == "lower_body_clothing")
        {
          for (var i = 0; i < obj.length; i++)
          {
            if (findGreatest(obj[i].properties.lower_body_clothing, 0) == value)
            {
              count++;
            }
          }
        }

        if (objecttype == "person_age")
        {
          for (var i = 0; i < obj.length; i++)
          {
            if (findGreatest(obj[i].properties.person_age, 0) == value)
            {
              count++;
            }
          }
        }

        if (objecttype == "person_gender")
        {
          for (var i = 0; i < obj.length; i++)
          {
            if (findGreatest(obj[i].properties.person_gender, 0) == value)
            {
              count++;
            }
          }
        }

        if (objecttype == "hand_position")
        {
          for (var i = 0; i < obj.length; i++)
          {
            if (findGreatest(obj[i].properties.hand_position, 0) == value)
            {
              count++;
            }
          }
        }

        if (objecttype == "head_gear")
        {
          for (var i = 0; i < obj.length; i++)
          {
            if (findGreatest(obj[i].properties.head_gear, 0) == value)
            {
              count++;
            }
          }
        }

        return count;
      }
