# This script will be used to bundle the created executables to a docker image

# Syntax to run the script is shown below:
# ./create_release.sh ImageName ImageTag

cp dockerignore.src_internal .dockerignore

echo "Creating build named: $1 version: $2"
docker build -f Dockerfile_src_internal -t $1:$2 .

#echo "Saving the image as a $1_$2.tar"
#docker save $1:$2 > $1_$2.tar
