#!/bin/bash

#python ./source/flask-app.py 5000 5555 5560 &




echo Application Running ...
python ./source/extract_pose.py &
sleep 10
python ./source/head_face_classifier.py &
sleep 4
python ./source/demo.py &
sleep 2
python ./source/person_classifiers_accumulator.py &
sleep 2
python ./source/sink.py &
python ./source/get_face_box.py &
python ./source/get_head_dir.py &
sleep 1
python ./source/image_input.py

