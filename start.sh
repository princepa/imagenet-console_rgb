#!/bin/bash

echo Application Running ...

./build/demo.exe &
sleep 5
./build/extract_pose.exe &
sleep 8
./build/head_face_classifier.exe &
sleep 10
./build/image_input.exe &
./build/person_classifiers_accumulator.exe &
sleep 2
./build/get_face_box.exe &
./build/get_head_dir.exe &
sleep 2
./build/sink.exe
