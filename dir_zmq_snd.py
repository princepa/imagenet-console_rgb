#! /usr/bin/python
# COPYRIGHT FUJITSU LIMITED 2018
# AUTHOR : Shubham Jain
# Date : April, 2, 2018


import os
import sys

import zmq
import numpy as np
import time
import cv2
import json
import ast


usage = "USAGE: python {} port_send port_recv img_dir".format(sys.argv[0])

if len(sys.argv)  != 4:
    print(usage)
    sys.exit("Usage Error")


port_send = str(sys.argv[1])
port_recv = str(sys.argv[2])
dir_path =str(sys.argv[3])
TIMEOUT = 30000  # zmq connection timeout in ms

context = zmq.Context()
send_socket = context.socket(zmq.PUSH)
send_socket.SNDTIMEO = TIMEOUT
send_socket.connect("tcp://127.0.0.1:"+port_send)


rcv_socket = context.socket(zmq.PULL)
rcv_socket.RCVTIMEO = TIMEOUT
rcv_socket.connect("tcp://127.0.0.1:"+port_recv)

# location to save the json results 
RESULT_FOLDER = 'results'
count = 0
image_list = os.listdir(dir_path)
img_format = ("jpg","jpeg","png")            
# file path 
#file_path = "test_image.jpg" 
first_send_time = None
last_send_time = None

for img_name in image_list:
    if not img_name.endswith(img_format):
        continue
    if first_send_time is None:
        first_send_time = time.time()
    count += 1
    last_img_time = time.time()
    full_path = os.path.join(dir_path,img_name)
    img = cv2.imread(full_path)
    filename = os.path.basename(img_name)    
    # required in the json 
    md = dict(
        # shape and dtype is used by api to reconstruct image  
        dtype=str(img.dtype),
        shape=img.shape,
        filename=filename,
        # time_stamp is used by api to calculate message Timeout
        time_stamp=time.time()
    )
    
    # Send json and Image to the api
    # send json before image 
    try:
        send_time = time.time()
        send_socket.send_json(md)
        print("{} json sent".format(filename))
        send_socket.send(img)
        print("{} Image sent".format(filename))
    except zmq.error.Again:
        print("Socket Connection Error")
        sys.exit("unable to send Image in {} ms...check receiver".format(TIMEOUT))
    
    
    # Receive the response from the api
    
    try:
        print("waiting for response ...")
        result = rcv_socket.recv_json()
        rcvd_json_filename = result["filename"]
        print("{} json received".format(rcvd_json_filename))
        print("total time taken: {} ".format(time.time()-send_time))
    except zmq.error.Again:
        print("No response in {}ms".format(TIMEOUT    ))
        sys.exit("Response Timed Out \n Try Again")
    
    
    # save the response in json in the RESULT_FOLDER
    #try:
    #    out_path = os.path.join(RESULT_FOLDER, filename.split(".")[-2] + ".json")
    #    with open(out_path, 'w') as f:
    #        json.dump(result, f, indent=4, ensure_ascii=False)
    #
    #    result = ast.literal_eval(json.dumps(result))
    #except Exception as e:
    #    print(str(e))
    #    print("error saving json {}".format(rcvd_json_filename))
    
    
    # print the received result 
    print(result)
    last_recv_time = time.time()




print("{} images processed in {} ".format(count,last_recv_time - first_send_time))
