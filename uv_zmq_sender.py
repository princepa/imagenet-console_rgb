#! /usr/bin/python
# AUTHOR : Shubham Jain
# Date : July 26, 2018


import os
import sys

import zmq
import numpy as np
import time
import cv2
import json
import ast


usage = "USAGE: python {} port_send img_dir".format(sys.argv[0])

if len(sys.argv)  != 3:
    print(usage)
    sys.exit("Usage Error")


port_send = str(sys.argv[1])

TIMEOUT = 10000  # zmq connection timeout in ms
COUNT = 100     # number of times to send
DELAY = 0.1      # time delay in seconds

context = zmq.Context()
send_socket = context.socket(zmq.PUSH)
send_socket.SNDTIMEO = TIMEOUT
send_socket.connect("tcp://127.0.0.1:"+port_send)


            
# file path 
file_path = "test_image.jpg" 
img_dir = sys.argv[2]
img_format = ("jpg","jpeg","png")

file_list = os.listdir(img_dir)
i = 0

for files in file_list:
    if not files.endswith(img_format):
        continue
    
    i += 1
    if DELAY > 0:
        time.sleep(DELAY)
    img_path = os.path.join(img_dir,files)
    # read the image in memory
    img = cv2.imread(img_path)
    filename = files

    # required in the json 
    md = dict(
        # shape and dtype is used by api to reconstruct image  
        dtype=str(img.dtype),
        shape=img.shape,
        filename=filename,
        # time_stamp is used by api to calculate message Timeout
        time_stamp=time.time()
    )
    
    # Send json and Image to the api
    # send json before image 
    try:
        send_socket.send_json(md)
        print("{} json sent".format(filename))
        send_socket.send(img)
        print("{} Images sent, #: {}".format(filename,i))
    except zmq.error.Again:
        print("Socket Connection Error")
        sys.exit("unable to send Image in {} ms...check receiver".format(TIMEOUT))

print("{} files sent".format(i))

