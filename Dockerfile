# Used for creating the release docker which contains the deliverables with executables.

#FROM ubuntu:16.04
FROM shubjain26/shub_tx2:tempcaffe_27june_sharvil
#FROM nvidia/cuda:8.0-cudnn7-devel-ubuntu16.04
MAINTAINER Uncanny Vision "fujitsu-support@uncannyvision.com"

COPY . /app
WORKDIR /app

#RUN rm -r /opt/darknet
#RUN rm -r /opt/caffe

EXPOSE 5555
EXPOSE 5560

# RUN pip install -r requirements.txt

ENTRYPOINT ["./start.sh"]
#ENTRYPOINT ["/bin/bash"]
