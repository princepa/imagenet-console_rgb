#!/bin/sh

apt-get install --yes curl
curl --silent --location https://deb.nodesource.com/setup_8.x | bash -
apt-get install --yes nodejs
apt-get install --yes build-essential
