#! /usr/bin/python
# COPYRIGHT FUJITSU LIMITED 2018
# AUTHOR : Shubham Jain
# Date : April, 23, 2018


import os
import sys

import zmq
import numpy as np
import time
import cv2
import json
import ast


usage = "USAGE: python {} port_recv results_dir".format(sys.argv[0])

if len(sys.argv)  != 3:
    print(usage)
    sys.exit("Usage Error")


port_recv = str(sys.argv[1])

TIMEOUT = 20000  # zmq connection timeout in ms

context = zmq.Context()

rcv_socket = context.socket(zmq.PULL)
rcv_socket.RCVTIMEO = TIMEOUT
rcv_socket.connect("tcp://127.0.0.1:"+port_recv)

# location to save the json results 
RESULT_FOLDER = sys.argv[2]


count = 0  #number of received messages
first_time = None
last_time = None

while True:
    # Receive the response from the api

    try:
        print("waiting for response ...")
        result = rcv_socket.recv_json()
        count += 1
        if first_time is None:
            first_time = time.time()
        last_time = time.time()
        rcvd_json_filename = result["filename"]
        print("{} json received, count: {}".format(rcvd_json_filename,count))
    except zmq.error.Again:
        print("No response in {}ms".format(TIMEOUT))
        break        



    # save the response in json in the RESULT_FOLDER
   # try:
   #     out_path = os.path.join(RESULT_FOLDER, os.path.basename(rcvd_json_filename).split(".")[-2] + ".json")
   #     with open(out_path, 'w') as f:
   #         json.dump(result, f, indent=4, ensure_ascii=False)

   #     result = ast.literal_eval(json.dumps(result))
   # except Exception as e:
   #     print(str(e))
   #     print("error saving json {}".format(rcvd_json_filename))


    # print the received result 
   # print(result)
	
if first_time and last_time:
    total_time = last_time - first_time
    print("TIMOUT")
    print("# received messages: {}".format(count))
    print("total time : {}".format(total_time))
    print("FPS: {}".format(count/total_time))

else:
    sys.exit("Response Timed Out \n Try Again")



