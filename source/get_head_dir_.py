import keras
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.optimizers import SGD

from sklearn.preprocessing import normalize
import numpy as np
import pickle
import random


config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.0005
set_session(tf.Session(config=config))

model_path = "models/head_direction/head_dir.h5"

model = Sequential()
# Dense(64) is a fully-connected layer with 64 hidden units.
# in the first layer, you must specify the expected input data shape:
# here, 20-dimensional vectors.
model.add(Dense(64, activation='relu', input_dim=10))
model.add(Dropout(0.5))
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(5, activation='softmax'))

#model.summary()

sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss='categorical_crossentropy',
              optimizer=sgd,
              metrics=['accuracy'])

model.load_weights(model_path)
#print(model)

#Input format  = [Left eyex, Left eyey, Righteyex, Righteyey, Leftearx, Lefteary, Rightearx, Righteary, Nosex, Nosey]
#[15,14,17,16,0]
#INPUT = [[210.381000, 491.658000, 414.894000, 447.687000, 217.097000, 679.170000, 654.355000, 603.780000, 276.932000, 602.619000]]


#Output = [Front, Top, Down, Left, Right]


def get_keypoints_head_dir(keypoints):
    head_dir_keypoints = [keypoints[15],keypoints[14],keypoints[17],keypoints[16],keypoints[0]]
    out_list = []
    for points in head_dir_keypoints:
        out_list.append(points[0])
        out_list.append(points[1])
    
    return out_list

def get_head_dir(keypoints):
    in_points = get_keypoints_head_dir(keypoints)
    norm_in_points = normalize(in_points, norm='l2', axis=1)   
    y_pred = model.predict_classes(norm_in_points)
    
    print(y_pred)
    return y_pred 
