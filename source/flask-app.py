#! /usr/bin/python

# AUTHOR : Shubham Jain
# Date : April, 16, 2018

usage="python flask-app-v2.py [port_flask=5000] [port_send=5555] [port_rcv=5560] [log]"


import os
import sys
from flask import Flask, request, redirect, flash, jsonify, Response
from werkzeug.utils import secure_filename

import zmq
import numpy as np
import time
import cv2
import json
import ast

TIMEOUT = 10000  # zmq connection timeout in ms


# FLASK PORT
if len(sys.argv) > 1:
    flask_port = int(sys.argv[1]) #flask port 
else :
    flask_port = 5000


# ZMQ PORTS
if len(sys.argv) > 2:
    snd_port = sys.argv[2] #send to demo
else :
    snd_port = str(5555)

if len(sys.argv) > 3:
    rcv_port = sys.argv[3] #receive from sink
else :
    rcv_port = str(5560)

if len(sys.argv) > 4 and sys.argv[4] == 'log':
    logging = True
else:
    logging = False


context = zmq.Context()
send_socket = context.socket(zmq.PUSH)
# send_socket.SNDTIMEO = TIMEOUT
send_socket.connect("tcp://127.0.0.1:" +snd_port)


rcv_socket = context.socket(zmq.PULL)
rcv_socket.RCVTIMEO = TIMEOUT
rcv_socket.connect("tcp://127.0.0.1:" +rcv_port)




RESULT_FOLDER = 'results'
UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = {'jpg', 'jpeg', 'png'}

app = Flask(__name__)
app.secret_key = 'some_secret'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # print("filename: ",file.filename)
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            # append data and time to the filename
            filename = time.strftime("%d_%h_%H_%M_%S__") + filename

            file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(file_path)

            t0 = time.time()
            img = cv2.imread(file_path)
            t_read = time.time() - t0
            md = dict(
                dtype=str(img.dtype),
                shape=img.shape,
                filename=filename,
                time_stamp=time.time()
            )

            try:
                t1 = time.time()
                send_socket.send_json(md)
                t_jsonsend = time.time() - t1
                print("flask. {} json sent".format(filename))
                t2 = time.time()
                print("flask. json send time: {}".format(t2))
                send_socket.send(img)
                t_imgsend = time.time() - t2
                t_sent = time.time()
                print("flask. {} Image sent".format(filename))
                print("flask. json send time: {}".format(t_sent))
            except zmq.error.Again:
                print("flask. Socket Connection Error")
                return "flask. Image dropped \n Try Again"

            try:
                print("flask. waiting for response ...")
                t3 = time.time()
                result = rcv_socket.recv_json()
                t_jsonrcv = time.time()
                t_jsonrcv_diff = t_jsonrcv - t3
                rcvd_json_filename = result["filename"]
                print("flask. {} json received".format(rcvd_json_filename))
            except zmq.error.Again:
                print("flask. No response in {}ms".format(TIMEOUT    ))
                return "flask. Response Timed Out \n Try Again"

            try:
                out_path = os.path.join(RESULT_FOLDER, filename.split(".")[-2] + ".json")
                t4 = time.time()
                with open(out_path, 'w') as f:
                    json.dump(result, f, indent=4, ensure_ascii=False)
                t_jsonsave = time.time() - t4
                t_end = time.time()
                result = ast.literal_eval(json.dumps(result))
            except Exception as e:
                print(str(e))
                print("flask. error saving json {}".format(rcvd_json_filename))

            if logging:
                print("image shape : {}".format(img.shape))
                print("file read time : {}".format(t_read))
                print("json send time : {}".format(t_jsonsend))
                print("image send time : {}".format(t_imgsend))
                print("json receive time : {}".format(t_jsonrcv_diff))
                print("inference time : {}".format(t3 - t_sent))
                print("json save time : {}".format(t_jsonsave))
                print("total time : {}".format(t_end - t0))
                print(" inference + zmq (result_receive_time - send_timestamp): {}s:".format(t_jsonrcv-md["time_stamp"]))
            return jsonify(result)
            #return Response(json.dumps(result),  mimetype='application/json')
            #return json.dumps(result,indent=4)


    return '''    
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    '''


@app.route('/', methods=['GET'])
def landing():
    return """
    <title>HomePage</title>
    <h3> Uncanny Vision </h3>
    <p> <a href ="/upload">click here</a> to upload image and process </p>
    """


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=flask_port)
