#!/usr/bin/python

import json
import sys
import time
import zmq
import ast
# Read configuration flags
from config_parser import flags

# import matplotlib.pyplot as plt
# import numpy as np

HWM = 4
DELAY = 0.0
TIMEOUT = 0    # timout is ms


# Check License Validity
# Expiry Data : July-15-2018 -> Epoch time : 1531612800 (UTC)
# Expiry Data : Dec-15-2018 -> Epoch time : 1544832000 (UTC)
EXP = 1544832000

def check_license():
    if time.time() > EXP:
        sys.exit("License Expired")

def zmq_receiver(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PULL)
    if not TIMEOUT == 0:
        zmq_socket.RCVTIMEO = TIMEOUT           # timeout time to close connection in ms
    zmq_socket.rcvhwm = HWM
    zmq_socket.connect("tcp://127.0.0.1:"+str(port))
    #zmq_socket.bind("tcp://127.0.0.1:5555")
    # Start your result manager and workers before you start your producers
    return zmq_socket


def zmq_sender(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.sndhwm = HWM
    # zmq_socket.SNDTIMEO = TIMEOUT
    zmq_socket.bind("tcp://*:"+str(port))
    # Start your result manager and workers before you start your producers
    return zmq_socket


def result_collector():
    global TIMEOUT
    context = zmq.Context()

    # result from person_classifier
    results_receiver = zmq_receiver(5559)
    
    # result head_face_classifier
    results_receiver_hfc = zmq_receiver(6559)
    
    send_socket = zmq_sender(5560)

    global timings

    while True:
        check_license()
        print("sink waiting for person_cls json")
        
        md = results_receiver.recv_json()
        #print("json received by sink: ",md)

        print("sink. json received")
        md["time_log"]["sink_recv_person"] = time.time()
        
        time_log = md["time_log"]
        merged_time_log = time_log.copy()

        if DELAY > 0:
            time.sleep(DELAY)

        json_data = md["data"]
        combined_result = json_data
        md_hfc = {}
        
        if flags["pose"]:
            print("sink. waiting for hfc json")
            md_hfc = results_receiver_hfc.recv_json()
            print("sink. received hfc json")
            merged_time_log["sink_recv_hfc"] = time.time()
            #md['ts_sink_recv'] = time.time()
            merged_time_log.update(md_hfc["time_log"])
            hfc_data = md_hfc["data"]
            combined_result["heads"] = hfc_data["heads"]
            combined_result["faces"] = hfc_data["faces"]
        
        
        # check the image timestamp
        
        if time.time() - md["time_stamp"] > 30:
            print("image dropped")
            continue
        
        #print("total inference time: {}".format(time.time() - md["time_stamp"]))

        # out_path = out_dir_path + '/' + json_data["filename"].split(".")[-2] + ".json"

        # with open(out_path, 'w') as f:
        #     json.dump(json_data, f, indent=4, ensure_ascii=False)

        print("sink. {}.json saved".format(combined_result["filename"].split(".")[-2]))
        print("\n"*3)
        #print("combined json : ",combined_result)
        result = ast.literal_eval(json.dumps(combined_result))
        print(result)
        time_log_summary = {}
        try:
            # print(merged_time_log)
            if flags["profile_time"]:
                print(" !!! "*10)
                time_log_summary["zmq_img2pose"] = merged_time_log["img_recv_pose"] - merged_time_log["img_send_imginput"]
                time_log_summary["zmq_pose2hfc"] = merged_time_log["img_recv_hfc"] - merged_time_log["img_send_pose"]
                time_log_summary["zmq_hfc2sink"] = merged_time_log["sink_recv_hfc"] - merged_time_log["img_send_hfc"]
                time_log_summary["zmq_img2demo"] = merged_time_log["img_recv_demo"] - merged_time_log["img_send_imginput"]
                time_log_summary["zmq_demo2PCA"] = merged_time_log["img_send_demo"] - merged_time_log["img_recv_person_cls_acc"]
                time_log_summary["zmq_PCA2sink"] = merged_time_log["sink_recv_person"] - merged_time_log["img_send_person_cls_acc"]

                time_log_summary["pose_detection"] = merged_time_log["pose_detection"] if merged_time_log.get("pose_detection") else 0
                time_log_summary["face_gender_cls"] = merged_time_log["face_gender_cls"] if merged_time_log.get("face_gender_cls") else 0
                time_log_summary["face_eyeacc_cls"] = merged_time_log["face_eyeacc_cls"] if merged_time_log.get("face_eyeacc_cls") else 0
                time_log_summary["face_race_cls"] = merged_time_log["face_race_cls"] if merged_time_log.get("face_race_cls") else 0 
                time_log_summary["face_age_cls"] = merged_time_log["face_age_cls"] if merged_time_log.get("face_age_cls") else 0
                time_log_summary["headgear_cls"] = merged_time_log["headgear_cls"] if merged_time_log.get("headgear_cls") else 0
                time_log_summary["facemask_cls"] = merged_time_log["facemask_cls"] if merged_time_log.get("facemask_cls") else 0
                time_log_summary["headdir_cls"] = merged_time_log["headdir_cls"] if merged_time_log.get("headdir_cls") else 0

                time_log_summary["total_person_classifiers"] = merged_time_log["total_person_classifiers"] if merged_time_log.get("total_person_classifiers") else 0
                time_log_summary["aio_detection"] = merged_time_log["aio_detection"] if merged_time_log.get("aio_detection") else 0
                time_log_summary["psc_detection"] = merged_time_log["psc_detection"] if merged_time_log.get("hepsc_detectionaddir_cls") else 0

                print(" !!! "*10)
                for key in time_log_summary:
                    print("{} = {}".format(key,time_log_summary[key]))
                print(" !!! "*10)
        except KeyError as err:
            print("Key error : ",str(err))
            pass
        
        try:
            send_socket.send_json(combined_result)
        except zmq.error.Again:
            print("sink. Unable to send {}".format(TIMEOUT))
            continue

if __name__ == "__main__":
    check_license()
    try:
        result_collector()

    except zmq.error.Again:
        print("sink. no packet received in {} ms \n exiting {}...".format(TIMEOUT, sys.argv[0]))

        time.sleep(15)
        sys.exit("sink. zmq TIMEOUT")
