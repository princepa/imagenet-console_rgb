import ConfigParser

Config = ConfigParser.ConfigParser()


try:
    Config.read("config/uv-libra-config")
except Exception:
    print("unable to file config file : /app/config/uv-libra-config \
        using default config ")
    Config.read("uv-libra-config-default")
    pass

# Parse Detector Options
person = Config.getboolean("Detectors","person")
bag = Config.getboolean("Detectors","bag")
head = Config.getboolean("Detectors","head")
shelf = Config.getboolean("Detectors","shelf")
movable_obj = Config.getboolean("Detectors","movable_obj")
face = Config.getboolean("Detectors","face")


# Parse Detector Options
thresh_person = float(Config.getfloat("Thresholds","person"))
thresh_bag = float(Config.getfloat("Thresholds","bag"))
thresh_head = float(Config.getfloat("Thresholds","head"))
thresh_shelf = float(Config.getfloat("Thresholds","shelf"))
thresh_movable_obj = float(Config.getfloat("Thresholds","movable_obj"))
thresh_face = float(Config.getfloat("Thresholds","face"))

# Parse Logging config
#time_log = Config.getboolean("Logs","time_logging")

# Parse Detector_GPU options
gpu_all_in_one_det = int(Config.getint("Detectors_GPU","all_in_one_det"))
gpu_shelf_det = int(Config.getint("Detectors_GPU","shelf_det"))
gpu_head_face_det = int(Config.getint("Detectors_GPU","head_face_det"))

# Parse Classifier_GPU options
gpu_head_face_class = int(Config.getint("Classifiers_GPU","head_face_cls"))
gpu_person_class = int(Config.getint("Classifiers_GPU","person_cls"))

# Parse Classifier Options
# if face detection is true
if face:
    age = Config.getboolean("Classifiers","age")
    race = Config.getboolean("Classifiers","race")
    gender = Config.getboolean("Classifiers","gender")
    eye_acc = Config.getboolean("Classifiers","eye_acc")
    face_mask = Config.getboolean("Classifiers","face_mask")
else:
    age,race,gender,eye_acc, face_mask = False,False,False,False,False

# if head detection is true
if head:
    head_gear = Config.getboolean("Classifiers","head_gear")
else :
    head_gear = False

# if person detection is true
if person:
    hand_position = Config.getboolean("Classifiers","hand_position")
    upper_body_clothing = Config.getboolean("Classifiers","upper_body_clothing")
    lower_body_clothing = Config.getboolean("Classifiers","lower_body_clothing")
    person_age = Config.getboolean("Classifiers","person_age")
    person_gender = Config.getboolean("Classifiers","person_gender")
else:
    hand_position,upper_body_clothing,lower_body_clothing,person_gender,person_age= False,False,False,False,False



flags = {
"person": person,
"bag": bag,
"head": head,
"shelf": shelf,
"movable_obj": movable_obj,
"face": face,
"race": race,
"age": age,
"gender": gender,
"eye_acc": eye_acc,
"face_mask" : face_mask,
"head_gear": head_gear,
"hand_position" : hand_position,
"upper_body_clothing" : upper_body_clothing,
"lower_body_clothing" : lower_body_clothing,
"thresh_person" : thresh_person,
"thresh_bag" : thresh_bag,
"thresh_head" : thresh_head,
"thresh_shelf" : thresh_shelf,
"thresh_movable_obj" : thresh_movable_obj,
"thresh_face" : thresh_face,
"person_age" : person_age,
"person_gender" : person_gender,
"pose": face or head,
"gpu_all_in_one_det": gpu_all_in_one_det,
"gpu_shelf_det" : gpu_shelf_det,
"gpu_head_face_det" : gpu_head_face_det, 
"gpu_head_face_class" : gpu_head_face_class,
"gpu_person_class" : gpu_person_class,
"profile_time" : False
}





















