#!/usr/bin/python
# -*- coding: utf-8 -*-

# import caffe
# from python_wrapper import *

# Read configuration flags
from config_parser import flags

import cv2
import os
#import _init_paths
import string
import random
import sys
from uvdetect import *
import time
import zmq
import numpy as np
from json_utils import JSON_writer
import subprocess
import stat
# from matplotlib import pyplot as plt

print("started with configuration")
for flg in flags:
    print("{} : {}".format(flg,flags[flg]))

gpu_all_in_one_det = flags["gpu_all_in_one_det"]
gpu_shelf_det = flags["gpu_shelf_det"]

#os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   
#os.environ["CUDA_VISIBLE_DEVICES"]=str(gpu_shelf_det)

HWM = 4
COPY = False
TIMEOUT = 0  # zmq connection timeout in ms
DELAY = 0.0
THRESH_PERSON = flags["thresh_person"]
THRESH_BAG = flags["thresh_bag"]
THRESH_HEAD = flags["thresh_head"]
THRESH_SHELF = flags["thresh_shelf"]
THRESH_MOVABLE_OBJ = flags["thresh_movable_obj"]
FLAGS_ALL_IN_ONE = flags["movable_obj"] or flags["person"] or flags["bag"]

__DEBUG = False
MODEL_LOAD_DELAY = 2

# counter = 0
timer = {"n_img": 0, "strt_time": None, "end_time": None}
rcv_timings = []

time_log = {}
# Check License Validity
# Expiry Data : July-01-2018 -> Epoch time : 1530403200 (UTC)
# Expiry Data : July-15-2018 -> Epoch time : 1531612800 (UTC)
# Expiry Data : August-15-2018 -> Epoch time : 1534291200 (UTC)
# Expiry Data : Dec-15-2018 -> Epoch time : 1544832000 (UTC)
EXP = 1544832000

def check_license():
    if time.time() > EXP:
        sys.exit("License Expired")

def uvprint(log):
    if __DEBUG == True:
        print(log)
    else:
        pass


def zmq_receiver(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PULL)
    if not TIMEOUT == 0:
        zmq_socket.RCVTIMEO = TIMEOUT           # timeout time to close connection in ms
    zmq_socket.rcvhwm = HWM
    zmq_socket.connect("tcp://127.0.0.1:"+str(port))
    #zmq_socket.bind("tcp://127.0.0.1:5555")
    # Start your result manager and workers before you start your producers
    return zmq_socket


def zmq_sender(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.sndhwm = HWM
    # zmq_socket.SNDTIMEO = TIMEOUT
    zmq_socket.bind("tcp://127.0.0.1:"+str(port))
    # Start your result manager and workers before you start your producers
    return zmq_socket


def drawPoints(im, points):
    for point in points:
        x = point[:5]
        y = point[5:]
        for i in range(5):
            cv2.circle(im, (int(x[i]), int(y[i])), 2, (0, 0, 255), -1, 8)
    return im


def drawBoxes(im, boxes):
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]
    for i in range(x1.shape[0]):
        cv2.rectangle(im, (int(x1[i]), int(y1[i])), (int(x2[i]), int(y2[i])), (0, 255, 0), 1)
    return im



def correct_bbox(coords,img_shape):

    h = img_shape[0]
    w = img_shape[1]
    r = coords

    r[0] = max(0, r[0])
    r[1] = max(0, r[1])
    r[2] = min(w-1, r[2])
    r[3] = min(h-1, r[3])

    return r

def get_random_name():
    n = random.randint(2,10)
    name = "." + ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(n))
    return name


def get_model_path(model_name):
    model_paths = { 
        "aio_cfg_file":"models/all_in_one/aio.cfg.gpg",
        "aio_data_file":"models/all_in_one/aio.data.gpg",
        "aio_weights":"models/all_in_one/aio.weights.gpg",
        "psc_cfg_file":"models/product_shelf/psc.cfg.gpg",
        "psc_data_file":"models/product_shelf/psc.data.gpg",
        "psc_weights":"models/product_shelf/psc.weights.gpg" }
    return model_paths[model_name]


def get_popen_command(model_path,p_name):
    cmd = "gpg -d --passphrase uncanny {} > {}".format(model_path,p_name)
    uvprint(cmd)
    return cmd

def main():
    zmq_socket_recv = zmq_receiver(5557)
    zmq_socket_send = zmq_sender(5558)
  

    if FLAGS_ALL_IN_ONE : 
        # set gpu mode for all_in_one
        #os.environ["CUDA_VISIBLE_DEVICES"]=str(gpu_all_in_one_det)
        #print("CUDA VISIBLE DEVICES SET TO: {}".format(gpu_all_in_one_det))
        set_gpu(gpu_all_in_one_det)
        #print("gou set for AIO detector")
        aio_cfg_file = get_random_name()
        aio_data_file = get_random_name()
        aio_weights = get_random_name()
        
        # mode = 0600|stat.S_IFIFO
        os.mkfifo(aio_cfg_file)
        os.mkfifo(aio_data_file)
        os.mkfifo(aio_weights)
        cmd = get_popen_command(get_model_path("aio_cfg_file"),aio_cfg_file)
        p = subprocess.Popen(cmd,stderr=subprocess.PIPE,shell=True)
        cmd = get_popen_command(get_model_path("aio_data_file"),aio_data_file)
        p = subprocess.Popen(cmd,stderr=subprocess.PIPE,shell=True)
    
        cmd = get_popen_command(get_model_path("aio_weights"),aio_weights)
        p = subprocess.Popen(cmd,stderr=subprocess.PIPE,shell=True)
        
        time.sleep(MODEL_LOAD_DELAY)
        aio_net = load_net(aio_cfg_file, aio_weights, 0)
        # aio_boxes = make_network_boxes(aio_net)
        # aio_meta = load_meta(aio_data_file)
        
        os.unlink(aio_cfg_file)
        os.unlink(aio_data_file)
        os.unlink(aio_weights)
        uvprint("done : aio")

    while 1:
        pass
    # product_shelf_capacity (psc)
    if flags["shelf"]:
     
        # set gpu for shelf detector
        #os.environ["CUDA_VISIBLE_DEVICES"]=str(gpu_shelf_det)
        #print("CUDA VISIBLE DEVICES SET TO: {}".format(gpu_shelf_det))
        set_gpu(gpu_shelf_det)
        #print("gou set for shelf detector")
        # generate random pipe names
        psc_cfg_file = get_random_name()
        psc_data_file = get_random_name()
        psc_weights = get_random_name()
      
        mode = 0600|stat.S_IFIFO
        # create pipes for file read
        os.mkfifo(psc_cfg_file)
        os.mkfifo(psc_data_file)
        os.mkfifo(psc_weights)
      
        cmd = get_popen_command(get_model_path("psc_cfg_file"),psc_cfg_file)
        p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
      
        cmd = get_popen_command(get_model_path("psc_data_file"),psc_data_file)
        p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)

        cmd = get_popen_command(get_model_path("psc_weights"),psc_weights)
        p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)


        time.sleep(MODEL_LOAD_DELAY)
        #read data from pipes
        psc_net = load_net(psc_cfg_file, psc_weights, 0)
        psc_boxes = make_network_boxes(psc_net)
        psc_meta = load_meta(psc_data_file)

        os.unlink(psc_cfg_file)
        os.unlink(psc_data_file)
        os.unlink(psc_weights)

        uvprint("done : shelf")



    while True:
        check_license()
        timer["n_img"] += 1
        print("Demo waiting for json...")
        md_temp = zmq_socket_recv.recv_json()
        try:
            filename = md_temp['filename']
        except KeyError:
            print('KeyError : filename not found in the received json')
            print("message dropped")
            # to clear the subsequent image in the queue
            msg = zmq_socket_recv.recv(copy=COPY)

            continue

        time_log["img_recv_demo"] = time.time()

        # print("{} received".format(timer["n_img"]))
        print("{} json received by demo".format(filename))
        
        msg = zmq_socket_recv.recv(copy=COPY)
        print("{} Image received by demo".format(filename))

        try:
            if time.time() - md_temp["time_stamp"] > 10:
                print("image dropped")
                continue
        except KeyError:
            print('KeyError : time_stamp not found in the received json')
            print("message dropped")
            continue

        try:
            dtype = md_temp['dtype']
        except KeyError:
            print('KeyError : dtype not found in the received json')
            print("message dropped")
            continue

        try:
            shape = md_temp['shape']
        except KeyError:
            print('KeyError : shape not found in the received json')
            print("message dropped")
            continue

        # Reconstruct image
        buf = buffer(msg)
        img_matlab = np.frombuffer(buf, dtype=md_temp['dtype'])

        # json_data = md['data']
        img = img_matlab.reshape(md_temp['shape'])

        # faster method than above
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        if FLAGS_ALL_IN_ONE :
            THRESH_AIO = min(THRESH_PERSON,THRESH_BAG,THRESH_MOVABLE_OBJ)
            t0_aio = time.time()
            aio_detections = detect(aio_net, aio_meta, img, thresh=THRESH_AIO)
            time_log["aio_detection"] = time.time() - t0_aio # time taken by all_in_one detection
        if flags["shelf"]:
            t0_psc = time.time()
            psc_detections = detect(psc_net, psc_meta, img, thresh=THRESH_SHELF)
            time_log["psc_detection"] = time.time() - t0_psc # time takn by shelf detection
        json_writer = JSON_writer(filename)

        # attributes = {}

        if FLAGS_ALL_IN_ONE :
            bag_boxes = []
            bag_confs = []
            bag_types = []
            mobj_boxes = []
            mobj_confs = []
            mobj_types = []
            for det in aio_detections:
                points = det[2]
                attributes = {}
                # confidence = {}
                if det[0] == 'Person' and det[1] >= THRESH_PERSON:
                    r = show_image_predictions(img.copy(), points)
                    # confidence['People'] = float(det[1])
                    person_confidence = float(det[1])
                    # correcting bounding boxes
                    r = correct_bbox(r, img.shape)
    
                    # json_writer.addBox(r[0], r[1], r[2], r[3], "People", attributes, confidence)
                    json_writer.addPeople(r[0], r[1], r[2], r[3],person_confidence)
    
                elif (det[0] == 'Suitcase' or det[0] == 'Handbag' or det[0] == 'Backpack') and flags["bag"] and det[1] >= THRESH_BAG:
                    r = show_image_predictions(img.copy(), points)
    
                    # correcting bounding boxes
                    r = correct_bbox(r, img.shape)
                    bag_confidence = float(det[1])
                    bag_type = det[0]
                    if r not in bag_boxes:
                        bag_boxes.append(r)
                        bag_confs.append(bag_confidence)
                        bag_types.append(bag_type)
                    else:
                        indx = bag_boxes.index(r)
                        if bag_confidence > bag_confs[indx]:
                            bag_confs[indx] = bag_confidence
                            bag_types[indx] = bag_type
                    # bag_boxes.append({"bbox":r,"conf":float(det[1]),"name":det[0]})
                    # attributes['ObjectType'] = {det[0]: {}}
                    # confidence['ObjectType'] = float(det[1])
                    
                    # json_writer.addBox(r[0], r[1], r[2], r[3], "Object", attributes, confidence)
                    # Replaced Above line with below : "Object" -> det[0]
                    # json_writer.addBox(r[0], r[1], r[2], r[3], det[0], attributes, confidence)
                    # json_writer.addObject(r[0], r[1], r[2], r[3], det[0],confidence)
                
                elif (det[0] == 'Stroller' or det[0] == 'Wheelchair') and flags["movable_obj"] and det[1] >= THRESH_MOVABLE_OBJ:
                    
                    mod_points = det[2]
                    mod_r = show_image_predictions(img.copy(), mod_points)
                    # correcting bounding boxes
                    mod_r = correct_bbox(mod_r, img.shape)
                    mobj_confidence = float(det[1])
                    mobj_type = det[0]
                    if mod_r not in mobj_boxes:
                        mobj_boxes.append(mod_r)
                        mobj_confs.append(mobj_confidence)
                        mobj_types.append(mobj_type)
                    else:
                        indx = mobj_boxes.index(mod_r)
                        if mobj_confidence > mobj_confs[indx]:
                            mobj_confs[indx] = mobj_confidence
                            mobj_types[indx] = mobj_type
                    # json_writer.addObject(mod_r[0], mod_r[1], mod_r[2], mod_r[3], det[0],mobj_confidence)
            
            for det_bag in zip(bag_boxes,bag_confs,bag_types):
                json_writer.addObject(det_bag[0][0],det_bag[0][1],det_bag[0][2],det_bag[0][3], det_bag[2],det_bag[1])
            
            for det_mobj in zip(mobj_boxes,mobj_confs,mobj_types):
                json_writer.addObject(det_mobj[0][0],det_mobj[0][1],det_mobj[0][2],det_mobj[0][3], det_mobj[2],det_mobj[1])
                         
    
        if flags["shelf"]:
            for psc_det in psc_detections:
                psc_points = psc_det[2]
                attributes = {}
                psc_confidence = float(psc_det[1])
                psc_r = show_image_predictions(img.copy(), psc_points)
                psc_r = correct_bbox(psc_r, img.shape)
                json_writer.addObject(psc_r[0], psc_r[1], psc_r[2], psc_r[3],"Empty-Shelf",psc_confidence)
                # print(psc_det)


        json_writer.write_to_json_v4_1()
        json_data = json_writer.json_data
        uvprint(json_data)
        # print("num of heads",len(json_data["heads"]))

        if DELAY > 0:
            time.sleep(DELAY)

        time_log["img_send_demo"] = time.time()
        md = dict(
            dtype=str(img.dtype),
            shape=img.shape,
            data=json_data,
            imgs_sent=timer['n_img'],
            time_stamp=md_temp["time_stamp"],
            time_log = time_log
        )
        
        # print(json_data)
        try:
            zmq_socket_send.send_json(md)
            zmq_socket_send.send(img_matlab, copy=COPY)

        except zmq.error as error:
            print("Demo. Error : {}, {} image dropped by {}".format(str(error),filename,sys.argv[0]))
            continue


if __name__ == "__main__":
    check_license()
    try:
        main()
    except zmq.error.Again:

        if timer["end_time"] and timer["strt_time"]:
            print("Demo. time to send {} images {} s".format(timer["n_img"], timer["end_time"] - timer["strt_time"]))

        time.sleep(10)
        sys.exit("Demo. zmq TIMEOUT")
