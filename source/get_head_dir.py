#get head direction from the keypoints

import os
import sys
import cv2
import json as JSON 
import numpy as np
import random 
from random import shuffle
import pickle
from config_parser import flags
import zmq

# Keras imports

from keras import backend as K
   
import keras
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session,get_session
from keras.models import Sequential 
from keras.layers import Dense
from keras.layers import Flatten, Reshape
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
from keras.models import load_model

#######################################################################
HWM = 4
COPY = False
TIMEOUT = 0  # zmq connection timeout in ms
head_dir_list = ["Straight","Bottom","Left","Right","Back"]
gpu_head_face_det = flags["gpu_head_face_det"]


def zmq_receiver(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PULL)
    if not TIMEOUT == 0:
        zmq_socket.RCVTIMEO = TIMEOUT           # timeout time to close connection in ms
    zmq_socket.rcvhwm = HWM
    zmq_socket.connect("tcp://127.0.0.1:"+str(port))
    #zmq_socket.bind("tcp://127.0.0.1:5555")
    # Start your result manager and workers before you start your producers
    return zmq_socket


def zmq_sender(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.sndhwm = HWM
    # zmq_socket.SNDTIMEO = TIMEOUT
    zmq_socket.bind("tcp://127.0.0.1:"+str(port))
    # Start your result manager and workers before you start your producers
    return zmq_socket


def get_keypoints_head(keypoints,image_size):
    head_keypoints = keypoints[0] + keypoints[14] + keypoints[15] + keypoints[16] + keypoints[17]
    #head_keypoint_copy = head_keypoints[:]
    x_coords = [head_keypoints[i] for i in range(0,len(head_keypoints),3)]
    y_coords = [head_keypoints[i] for i in range(1,len(head_keypoints),3)]
    sum_x = sum(x_coords)
    sum_y = sum(y_coords)
    
    visible_points = sum(head_keypoints[i] > 0.0 for i in range(2,len(head_keypoints),3))
    center_x = sum_x/visible_points
    center_y = sum_y/visible_points
    normalised_points = head_keypoints[:]  
    for i in range(2,len(head_keypoints),3):
        if normalised_points[i] > 0.0:
            normalised_points[i] = 1
            normalised_points[i-2] = normalised_points[i-2] - center_x
            normalised_points[i-1] = normalised_points[i-1] - center_y
    return normalised_points

def get_head_dir(keypoints,image_size):
    #model_head = load_model(model_path_head)
    # keypoints = 0 nose
    #             14 Reye, 15 L eye, 16 Rear ,17 L ear
    #             1 neck

    # for head box : 0,14,15,16,17

    input_head_keypoints = get_keypoints_head(keypoints,image_size)
    input_head_keypoints = np.array([input_head_keypoints])
    y_pred = model_head_dir.predict(input_head_keypoints, batch_size=1, verbose = 0).tolist()
    y_pred_class = model_head_dir.predict_classes(input_head_keypoints)
    return y_pred[0]

model_path_head_dir = "models/head_dir/head_dir.h5"

with K.tf.device('/gpu:' + str(gpu_head_face_det)):
    config = tf.ConfigProto(intra_op_parallelism_threads=4,\
           inter_op_parallelism_threads=4, allow_soft_placement=True,\
           device_count = {'GPU' : 0})
    config.gpu_options.per_process_gpu_memory_fraction = 0.0005
    session_head_dir = tf.Session(config=config)
    session_head_dir.run(tf.global_variables_initializer())
    K.set_session(session_head_dir)
model_head_dir= load_model(model_path_head_dir)
    
#    model = Sequential()
#    model.add(Dense(32, input_dim=15,activation='tanh'))
#    model.add(BatchNormalization())
#    model.add(Dense(32, activation='tanh'))
#    model.add(BatchNormalization())
#    model.add(Dense(32, activation='tanh'))
#    model.add(BatchNormalization())
#    model.add(Dense(5, activation='softmax'))
#    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
#    model.compile(loss='categorical_crossentropy',optimizer="adam",metrics=['accuracy'])
#    model.load_weights(model_path_head_dir)

zmq_socket_recv = zmq_receiver(6553)
zmq_socket_send = zmq_sender(6554)



if __name__ == '__main__':
    while True:
        print("head_dir waiting for msg...")
        md_temp = zmq_socket_recv.recv_json()
        keypoints = md_temp["keypoints"]
        image_size =md_temp["image_size"]
        #print("msg received by head_box: ",md_temp)
        
        head_dir = get_head_dir(keypoints,image_size)
        head_dir_result = {name:head_dir[i] for i,name in enumerate(head_dir_list)}
        #print("head_dir_result: ",head_dir_result)
        result = {"head_dir":head_dir_result}
        zmq_socker_send = zmq_socket_send.send_json(result)
        print("msg sent by head_dir")
   
        
