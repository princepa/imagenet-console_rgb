#!/usr/bin/python
import os
import stat 
import sys
import zmq
import random
import numpy as np
import string
import cv2
import time
import pdb
import _init_paths
from subprocess import Popen, PIPE
import subprocess
# Read configuration flags
from config_parser import flags
from tempfile import mkstemp

HEAD_FACE_CLS_FLAG = flags["race"] or flags["age"] or flags["gender"] or flags["eye_acc"] or flags["face_mask"] or flags["head_gear"]

if HEAD_FACE_CLS_FLAG:
    import caffe
    caffe.set_mode_gpu()
    caffe.init_log(1)
    gpu_head_face_class = flags["gpu_head_face_class"]
    caffe.set_device(gpu_head_face_class)

#from get_face_box import *
if flags["head"]:
    from get_head_box import *

#from get_head_dir import *

#os.environ["CUDA_VISIBLE_DEVICES"] = "0"

TIMEOUT = 0    # timout is ms
HWM = 4
DELAY = 0.0
MODEL_LOAD_DELAY = 5
__DEBUG = False




def uvprint(log):
    if __DEBUG == True:
        print(log)
    else:
        pass

# Check License Validity
# Expiry Data : July-01-2018 -> Epoch time : 1530403200 (UTC)
# Expiry Data : July-05-2018 -> Epoch time : 1531612800 (UTC)
# Expiry Data : August-15-2018 -> Epoch time : 1534291200 (UTC)
# Expiry Data : Dec-15-2018 -> Epoch time : 1544832000 (UTC)
EXP = 1544832000

def check_license():
    if time.time() > EXP:
        sys.exit("License Expired")


def get_random_name():
    n = random.randint(2,10)
    name = "." + ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(n))
    return name


def get_model_path(model_name):
    model_paths = { "deploy_gender":"models/gender/gender.prototxt.gpg",
                "model_gender":"models/gender/gender.caffemodel.gpg",
                "deploy_spec":"models/eye_acc/eye_acc.prototxt.gpg",
                "model_spec":"models/eye_acc/eye_acc.caffemodel.gpg",
                "deploy_age":"models/age/age.prototxt.gpg",
                "model_age":"models/age/age.caffemodel.gpg",
                "deploy_race":"models/race/race.prototxt.gpg",
                "model_race":"models/race/race.caffemodel.gpg",
                "deploy_fm":"models/face_mask/face_mask.prototxt.gpg",
                "model_fm":"models/face_mask/face_mask.caffemodel.gpg",
                "deploy_hgr":"models/head_gear/head_gear.prototxt.gpg",
                "model_hgr":"models/head_gear/head_gear.caffemodel.gpg" }
              
    return model_paths[model_name]

def get_popen_command(model_path,p_name):
    cmd = "gpg -d --passphrase uncanny {} > {}".format(model_path,p_name)
    uvprint(cmd)
    return cmd

def zmq_receiver(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PULL)
    if not TIMEOUT == 0:
        zmq_socket.RCVTIMEO = TIMEOUT           # timeout time to close connection in ms
    zmq_socket.rcvhwm = HWM
    zmq_socket.connect("tcp://127.0.0.1:"+str(port))
    #zmq_socket.bind("tcp://127.0.0.1:5555")
    # Start your result manager and workers before you start your producers
    return zmq_socket


def zmq_sender(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.sndhwm = HWM
    # zmq_socket.SNDTIMEO = TIMEOUT
    zmq_socket.bind("tcp://127.0.0.1:"+str(port))
    # Start your result manager and workers before you start your producers
    return zmq_socket


face_box_sender = zmq_sender(6557)
face_box_receiver = zmq_receiver(6558)
head_dir_sender = zmq_sender(6553)
head_dir_receiver = zmq_receiver(6554)


nets = {}
# Caffe model Paths

def classify(img, net_name, scale_param=0.00390625,channels=1):
    # print(img.shape)
    # print(bl)
    global nets
    net2 = nets[net_name]
    img = img.astype(dtype=float) * scale_param
    shape = net2.blobs['data'].data.shape[2:]
    net2.blobs['data'].reshape(1, channels, shape[0], shape[1])


    if channels==1:
        net2.blobs['data'].data[...] = np.transpose(img[:, :, None].astype(dtype=np.float), (2, 0, 1))
    elif channels==3:
        net2.blobs['data'].data[...] = np.transpose(img.astype(dtype=np.float), (2, 0, 1))

    output = net2.forward()
    return output['probs'][0]

def get_head_direction(keypoints,img_shape):
    global head_dir_sender, head_dir_receiver
    head_keypoints = keypoints[0] + keypoints[14] + keypoints[15] + keypoints[16] + keypoints[17]
    #x_coords = [head_keypoints[i] for i in range(0,len(head_keypoints),3)]
    #y_coords = [head_keypoints[i] for i in range(1,len(head_keypoints),3)]
    #sum_x = sum(x_coords)
    #sum_y = sum(y_coords)
    visible_points = sum(head_keypoints[i] > 0.0 for i in range(2,len(head_keypoints),3))
    
    if visible_points >= 1 :
        keypoints_json = {"keypoints": keypoints.tolist(),"image_size":img_shape}
        head_dir_sender.send_json(keypoints_json)
        head_dir = head_dir_receiver.recv_json()
        return head_dir["head_dir"]
    else:
        return None    


def keypoints_to_person_head(keypoints,img_shape):
    #print(img_shape)

    conf = (keypoints[1][2] + keypoints[0][2] + keypoints[14][2] + keypoints[15][2] + keypoints[16][2] + keypoints[17][2])/6
    visible_points = 0
    for key in keypoints:
       if all(key) > 0.0:
            visible_points += 1
    
    if visible_points >=2 and conf > flags["thresh_head"]:
        bbox = get_head_box(keypoints,img_shape)#.tolist()
        bbox.append(conf)
        return bbox
    else :
        return None

def keypoints_to_person_face(keypoints,img_shape):
    global face_box_sender, face_box_receiver
    
    conf = (keypoints[0][2] + keypoints[14][2] + keypoints[15][2] + keypoints[16][2] + keypoints[17][2])/5
    visible_points = 0
    for key in keypoints:
       if all(key) > 0.0:
            visible_points += 1
    
    if visible_points >= 2 and conf > flags["thresh_face"]: 
        if keypoints[0][2] > 0 or keypoints[14][2] > 0 or keypoints[15][2] > 0 :
            keypoints_json = {"keypoints": keypoints.tolist(),
                                   "image_size":img_shape}
            face_box_sender.send_json(keypoints_json)
            box_json = face_box_receiver.recv_json()
            bbox = box_json["box"]
            bbox.append(conf)
            return bbox
        else:
            return None
    else:
        return None



# Check if Face flag is True
if flags["gender"]:
    deploy_gender = get_random_name()
    model_gender = get_random_name()
    os.mkfifo(deploy_gender)
    _,model_gender = mkstemp(model_gender)
    cmd = get_popen_command(get_model_path("deploy_gender"),deploy_gender)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    cmd = get_popen_command(get_model_path("model_gender"),model_gender)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    time.sleep(MODEL_LOAD_DELAY)
    net_gender = caffe.Net(deploy_gender, model_gender, caffe.TEST)
    os.unlink(deploy_gender)
    os.unlink(model_gender)
    nets["net_gender"] = net_gender
    uvprint("done : gender")


# Check if eyeacc flag is True
if flags["eye_acc"]:
    deploy_spec = get_random_name()
    model_spec = get_random_name()
    os.mkfifo(deploy_spec)
    _,model_spec = mkstemp(model_spec)
    cmd = get_popen_command(get_model_path("deploy_spec"),deploy_spec)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    cmd = get_popen_command(get_model_path("model_spec"),model_spec)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    time.sleep(MODEL_LOAD_DELAY)
    net_spec = caffe.Net(deploy_spec, model_spec, caffe.TEST)
    os.unlink(deploy_spec)
    os.unlink(model_spec)
    nets["net_spec"] = net_spec
    uvprint("done : eye_acc")


# Check if age flag is True
if flags["age"]:
    deploy_age = get_random_name()
    model_age = get_random_name()
    os.mkfifo(deploy_age)
    _,model_age = mkstemp(model_age)
    cmd = get_popen_command(get_model_path("deploy_age"),deploy_age)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    cmd = get_popen_command(get_model_path("model_age"),model_age)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    time.sleep(MODEL_LOAD_DELAY)
    net_age = caffe.Net(deploy_age, model_age, caffe.TEST)
    os.unlink(deploy_age)
    os.unlink(model_age)
    nets["net_age"] = net_age
    uvprint("done : age")


# Check if race flag is True
if flags["race"]:
    deploy_race = get_random_name()
    model_race = get_random_name()
    os.mkfifo(deploy_race)
    _,model_race = mkstemp(model_race)
    cmd = get_popen_command(get_model_path("deploy_race"),deploy_race)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    cmd = get_popen_command(get_model_path("model_race"),model_race)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    time.sleep(MODEL_LOAD_DELAY)
    net_race = caffe.Net(deploy_race, model_race, caffe.TEST)
    os.unlink(deploy_race)
    os.unlink(model_race)
    nets["net_race"] = net_race
    uvprint("done : race")



# Check if face mask flag is True (fm)
if flags["face_mask"]:
    deploy_fm = get_random_name()
    model_fm = get_random_name()
    os.mkfifo(deploy_fm)
    _,model_fm = mkstemp(model_fm)
    cmd = get_popen_command(get_model_path("deploy_fm"),deploy_fm)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    cmd = get_popen_command(get_model_path("model_fm"),model_fm)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    time.sleep(MODEL_LOAD_DELAY)
    net_fm = caffe.Net(deploy_fm, model_fm, caffe.TEST)
    os.unlink(deploy_fm)
    os.unlink(model_fm)
    nets["net_fm"] = net_fm
    uvprint("done : face_mask")


# Check if head_gear flag is True (hgr)
if flags["head_gear"]:
    deploy_hgr = get_random_name()
    model_hgr = get_random_name()
    os.mkfifo(deploy_hgr)
    _,model_hgr = mkstemp(model_hgr)
    cmd = get_popen_command(get_model_path("deploy_hgr"),deploy_hgr)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    cmd = get_popen_command(get_model_path("model_hgr"),model_hgr)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    time.sleep(MODEL_LOAD_DELAY)
    net_hgr = caffe.Net(deploy_hgr, model_hgr, caffe.TEST)
    #net_hgr = caffe.Net("models/head_gear/head_gear.prototxt", "models/head_gear/head_gear.caffemodel", caffe.TEST)
    os.unlink(deploy_hgr)
    os.unlink(model_hgr)
    #uvprint("DEBUG")
    #uvprint(net_hgr.params["fc2_hg"][0].data)
    nets["net_hgr"] = net_hgr
    uvprint("done : hgr")

# nets = {"net_gender": net_gender, "net_spec": net_spec, "net_race": net_race, "net_age": net_age}

# age_list = ["0-10", "10-20", "20-50", "20-50", "20-50", "50+"]  ## old
age_list = ["0-9", "10-19", "20-50", "50+"]  # new list
race_list = ["South-Asian", "East-Asian", "Caucasian", "African"]
gender_list = ["Female", "Male"]
spec_list = ["None", "Specs", "Sunglasses"]

fm_list = ["No-Mask","Mask"]
hgr_list = ["None", "Cap/Hat/Cycle-Helmet", "Full-Helmet"]

head_dir_list = ["Straight","Bottom","Left","Right","Back"]
timer = {"n_img": 0, "strt_time": None, "end_time": None}




def consumer():
    global TIMEOUT

    consumer_receiver = zmq_receiver(6556)
    uvprint("head_face. receiver created")

    # send work
    consumer_sender = zmq_sender(6559)
    uvprint("head_face. sender created")

    while True:
        check_license()
        print("head_face. waiting for json")
        md = consumer_receiver.recv_json()
        filename = md["filename"]
        print("head_face. {} json received".format(filename))

        msg = consumer_receiver.recv()
        print("head_faceC. {} Image received".format(filename))

        md["time_log"]["img_recv_hfc"] = time.time()
        # check the image timestamp
        if time.time() - md["time_stamp"] > 10:
            print("head_faceC. Image dropped")
            continue

        # Reconstruct Image

        buf = buffer(msg)
        img = np.frombuffer(buf, dtype=md['dtype'])
        img_shape = md['shape']
        img = img.reshape(img_shape)

        #img_debug = img.copy()

        if flags["pose"]:
            poses = md["data"]["poses"]
            person_keypoints = np.array(md["data"]["poses"])
            person_keypoints_copy = np.array(md["data"]["poses"])
        
        md['data']['faces'] = []
        md['data']['heads'] = []
        
        #print("keypoints: ",person_keypoints)       
        #print("keypoints_copy: ",person_keypoints_copy)
        
        face_bbox = []
        head_bbox = []
        head_dir = [] 
        head_directions = []
        if flags["face"]:
            for person2 in person_keypoints_copy:
               # print("keypoints: ",person)
               # keypoints_json = {"keypoints": person2.tolist(),
               #                    "image_size":img_shape}
               # face_box_sender.send_json(keypoints_json)
               # box_json = face_box_receiver.recv_json()
               # box = box_json["box"]
               # print("face box received by hfc: ",box)
                #face_keypoints = [person2[0],person2[14],person2[15],person2[16],person2[17]]
                #print(face_keypoints)
                #for points in face_keypoints:
                #    cv2.circle(img_debug,(points[0],points[1]),1,2,color=(255,0,0),thickness=-1)

                box = keypoints_to_person_face(person2.copy(),img_shape)
                if box:
                    face_bbox.append(box)
                    #cv2.rectangle(img_debug,(int(box[0]),int(box[1])),(int(box[2]),int(box[3])),(0,0,255),2)
                #head_directions.append(get_head_direction(person))

        if flags["head"]:   
            for person1 in person_keypoints:
                #print("keypoints: ",person)
                box = keypoints_to_person_head(person1.copy(),img_shape)
                head_keypoints = [person1[1],person1[0],person1[14],person1[15],person1[16],person1[17]]
                #for points in head_keypoints:
                    #cv2.circle(img_debug,(int(points[0]),int(points[1])),2,(0,255,255),-1)

                if box:
                    head_bbox.append(box)
                    #cv2.rectangle(img_debug,(int(box[0]),int(box[1])),(int(box[2]),int(box[3])),(255,0,0),2)
                    head_dir = get_head_direction(person1.copy(),img_shape)
                    if head_dir:
                        head_directions.append(head_dir)
        
        #cv2.imwrite(os.path.join("debug",filename),img_debug)
          
        # face_bbox = []
        # if flags["face"]:
        #     face_bbox = np.array(md['face_bbox'])
        #     poses = md['poses']
        #     ratios = md['ratios']
        #     hd_conf = md['hd_conf']
            
        # points = md['points']  # not being used
        #curr_pose = -1
        
        # for face classifiers (age,race,gender,eye_acc,face_mask)
        if flags["face"]:
            for r in face_bbox:
                
                
                #print("bbox: ", r)
                # print("shape: ", img.shape)
                # continue
                #curr_pose += 1
                fc_conf = r[4] # face confidence value
    
                #r = r.astype(dtype=np.int)
                np_r = np.array(r)
                if ((np.any(np_r < 0)) or (np_r[3] - np_r[1] + 1 <= 20)
                    or (np_r[2] -np_r[0] + 1 <= 20) or (np_r[0] > img.shape[1]) 
                    or (np_r[1] > img.shape[0]) or (np_r[2] > img.shape[1]) 
                    or (np_r[3] > img.shape[0])):
                    print("head_faceC. Face skipped(below 20x20)")
                    continue
    
                #tmp_r = r.astype(int)
                tmp_r = r[:]
                tmp_h = r[3] - r[1]
                tmp_w = r[2] - r[0]
                # tmp_r extending crop by 20% and converting to int
                tmp_r[0] = max(0, int(tmp_r[0] - tmp_w / 5))
                tmp_r[1] = max(0, int(tmp_r[1] - tmp_h / 5))
                tmp_r[2] = min(img.shape[1] - 1, int(tmp_r[2] + tmp_w / 5))
                tmp_r[3] = min(img.shape[0] - 1, int(tmp_r[3] + tmp_h / 5))

                #tmp_r2 without extending crop
                tmp_r2 = r[:]
                tmp_r2[0] = max(0, int(tmp_r2[0]))
                tmp_r2[1] = max(0, int(tmp_r2[1]))
                tmp_r2[2] = min(img.shape[1] - 1, int(tmp_r2[2]))
                tmp_r2[3] = min(img.shape[0] - 1, int(tmp_r2[3]))
 
                in_img_crop_larger = cv2.resize(
                    cv2.cvtColor(img[tmp_r[1]:tmp_r[3] + 1, tmp_r[0]:tmp_r[2] + 1, :], cv2.COLOR_RGB2GRAY), (128, 128))
                 
                # in_img_crop = cv2.resize(cv2.cvtColor(img[r[1]:r[3] + 1, r[0]:r[2] + 1, :], cv2.COLOR_RGB2GRAY), (128, 128))
    
                # for race classifier
                in_img_crop_rgb = cv2.resize(img[tmp_r2[1]:tmp_r2[3] + 1, tmp_r2[0]:tmp_r2[2] + 1, :], (128, 128))
                # in_img_crop_rgb = cv2.cvtColor(in_img_crop_rgb,cv2.COLOR_RGB2BGR)
    
                # for eyewear classifier (64 x 64)
                in_img_crop_eye = cv2.resize(img[tmp_r2[1]:tmp_r2[3] + 1, tmp_r2[0]:tmp_r2[2] + 1, :], (64, 64))
    
                # for face_mask classifier (64 x 64, gray)
                in_img_crop_gray= cv2.cvtColor(in_img_crop_rgb,cv2.COLOR_RGB2GRAY)
    
                # cv2.imwrite("tmp.png",in_img_crop)u
    
                if flags["gender"]:
                    t0_gender = time.time()
                    gender_probs = classify(in_img_crop_larger, "net_gender")
                    md["time_log"]["face_gender_cls"] = time.time() - t0_gender

                if flags["eye_acc"]:
                    t0_eyeacc = time.time()
                    spec_probs = classify(in_img_crop_eye, "net_spec", channels=3)
                    md["time_log"]["face_eyeacc_cls"] = time.time() - t0_eyeacc

                if flags["race"]:
                    t0_race = time.time()
                    race_probs = classify(in_img_crop_rgb, "net_race", channels=3)
                    md["time_log"]["face_race_cls"] = time.time() - t0_race

                if flags["age"]:
                    t0_age = time.time()
                    age_probs = classify(in_img_crop_larger, "net_age")
                    md["time_log"]["face_age_cls"] = time.time() - t0_age

                #if flags["face_mask"]:
                #   fm_probs = classify(in_img_crop_gray, "net_fm")
    
                if DELAY > 0:
                    time.sleep(DELAY)
    
                # Merge Age probs for 4 classes
                if flags["age"]:
                    age_probs_new = [0, 0, 0, 0]
                    age_probs_new[0] = age_probs[0]
                    age_probs_new[1] = age_probs[1]
                    age_probs_new[2] = age_probs[2] + age_probs[3] + age_probs[4]
                    age_probs_new[3] = age_probs[5]
    
    
                attributes = {}
                # attributes["headdirection"] = poses[curr_pose]
                #attributes["headdirection"] = get_head_direction(in_img_crop_larger)
                           
               # hd_conf = { "straight":0.45285,"top":0.05526,"left":0.24585,"right":0.24604 } # TODO
    
    
                if flags["face"]:
                    faces = {
                        "coordinates": {"xmin": int(r[0]), "ymin": int(r[1]), "width": int(r[2] - r[0]), "height": int(r[3] - r[1])},
                        "properties": {},
                        "confidence" : float(round(fc_conf,4))
                    }
    
                if flags["gender"]:
                    faces["properties"]["gender"] = [{"name": gender_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(gender_probs)]
    
                if flags["eye_acc"]:
                    faces["properties"]["eye_acc"] = [{"name": spec_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(spec_probs)]
    
                if flags["age"]:
                    faces["properties"]["age"] = [{"name": age_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(age_probs_new)]
    
                if flags["race"]:
                    faces["properties"]["race"] = [{"name": race_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(race_probs)]
    
                #if flags["face_mask"]:
                #    faces["properties"]["face_mask"] = [{"name": fm_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(fm_probs)]
    
                #if flags["face"]:
                #    #faces["properties"]["headdirection"] = attributes["headdirection"]
                #    faces["properties"]["headdirection"] =  [{"name":direction_name,"confidence":float(round(hd_conf[direction_name],4))} for direction_name in hd_conf ]
    
                if flags["face"]:
                    md['data']['faces'].append(faces)  # appeding to bbox for final json data
                    #print(faces)

        if flags["head"]:
            for num,head in enumerate(head_bbox):
                hd_conf = head[4]
                #hdir_conf = { "Bottom":0.45285,"Straight":0.05526,"Left":0.24585,"Right":0.24604,"Back":0.0} # TODO
                hdir_conf = head_directions[num]
                heads = {
                        "coordinates": {"xmin": int(head[0]), "ymin": int(head[1]), "width": int(head[2] - head[0]), "height": int(head[3] - head[1])},
                        "properties": {},
                        "confidence" : float(round(hd_conf,4))
                    }
               # hd_r = [head["coordinates"]["xmin"],
               #     head["coordinates"]["ymin"],
               #     head["coordinates"]["xmin"] + head["coordinates"]["width"],
               #     head["coordinates"]["ymin"] + head["coordinates"]["height"]
               #     ]
                #print("head box: ",head)
                
                np_head = np.array(head)
                if ((np.any(np_head < 0)) or (np_head[3] - np_head[1] + 1 <= 20) or (np_head[2] - np_head[0] + 1 <= 20)
                    or (np_head[0] > img.shape[1]) or (np_head[1] > img.shape[0]) or (np_head[2] > img.shape[1])
                    or (np_head[3] > img.shape[0])):
                    print("head_faceC. Head skipped(below 20x20)")
                    continue

                hd_r = head[:]
                tmp_r = hd_r
                
                tmp_h = hd_r[3] - hd_r[1]
                tmp_w = hd_r[2] - hd_r[0]
    
                tmp_r[0] = max(0, int(tmp_r[0] ))
                tmp_r[1] = max(0, int(tmp_r[1] ))
                tmp_r[2] = min(img.shape[1] - 1, int(tmp_r[2] ))
                tmp_r[3] = min(img.shape[0] - 1, int(tmp_r[3] ))
    
                # for head gear classifier
                #print(img.shape)
                hd_img = img[tmp_r[1]:tmp_r[3] + 1, tmp_r[0]:tmp_r[2] + 1, :]
                #print(hd_img)
    
                cropped_head = cv2.cvtColor(cv2.resize(hd_img, (128, 128)),cv2.COLOR_RGB2GRAY)
                cropped_head_rgb = cv2.resize(hd_img, (128, 128))
                #cropped_head_64= cv2.resize(cropped_head, (64, 64))
                
                #print("cropped_head shape: ",cropped_head.shape)
    
                if flags["head_gear"]:
                    t0_headgear = time.time()
                    hgr_probs = classify(cropped_head_rgb, "net_hgr",channels=3)
                    heads["properties"]["head_gear"] = [{"name": hgr_list[indx], "confidence": float(round(conf,5))} for indx, conf in enumerate(hgr_probs)]
                    md["time_log"]["headgear_cls"] = time.time() - t0_headgear
                    
                if flags["face_mask"]:
                    t0_facemask = time.time()
                    fm_probs = classify(cropped_head_rgb, "net_fm",channels=3)
                    #print("fm_probs: ",fm_probs)
                    heads["properties"]["face_mask"] =[{"name": fm_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(fm_probs)]
                    md["time_log"]["facemask_cls"] = time.time() - t0_facemask
    
                if flags["head"]:
                    t0_headdir = time.time()
                    #faces["properties"]["headdirection"] = attributes["headdirection"]
                    heads["properties"]["headdirection"] =  [{"name":direction_name,"confidence":float(round(hdir_conf[direction_name],4))} for direction_name in hdir_conf ]
                    md["time_log"]["headdir_cls"] = time.time() - t0_headdir

                if flags["head"]:
                    md['data']['heads'].append(heads)  # appeding to bbox for final json data
    
    
        md["time_log"]["img_send_hfc"] = time.time()
        try:
            consumer_sender.send_json(md)
            #print(md["data"])
        except zmq.error.Again:
            print("Unable to send image dropped")
            continue

        timer["n_img"] += 1


if __name__ == "__main__":

    check_license()
    try:
        consumer()

    except zmq.error.Again:
        print("classifierC. no packet received in {} ms \n exiting {}...".format(TIMEOUT, sys.argv[0]))
        print("classifierC. {} images sent".format(timer["n_img"]))
        time.sleep(10)
        cv2.destroyAllWindows()
        sys.exit("classifierC. zmq TIMEOUT")
