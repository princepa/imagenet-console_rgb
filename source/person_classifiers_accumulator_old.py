#!/usr/bin/python
import os
import stat 
import sys
import zmq
import random
import numpy as np
import string
import cv2
import time
import pdb
import _init_paths
from subprocess import Popen, PIPE
import subprocess
# Read configuration flags
from config_parser import flags
from tempfile import mkstemp
import caffe
caffe.set_mode_gpu()
caffe.init_log(1)

TIMEOUT = 0    # timout is ms
HWM = 4
DELAY = 0.0
COPY = False
MODEL_LOAD_DELAY = 1
__DEBUG = False



def zmq_receiver(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PULL)
    if not TIMEOUT == 0:
        zmq_socket.RCVTIMEO = TIMEOUT           # timeout time to close connection in ms
    zmq_socket.rcvhwm = HWM
    zmq_socket.connect("tcp://127.0.0.1:"+str(port))
    #zmq_socket.bind("tcp://127.0.0.1:5555")
    # Start your result manager and workers before you start your producers
    return zmq_socket


def zmq_sender(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.sndhwm = HWM
    # zmq_socket.SNDTIMEO = TIMEOUT
    zmq_socket.bind("tcp://127.0.0.1:"+str(port))
    # Start your result manager and workers before you start your producers
    return zmq_socket


def uvprint(log):
    if __DEBUG == True:
        print(log)
    else:
        pass

# Check License Validity
# Expiry Data : July-01-2018 -> Epoch time : 1530403200 (UTC)
# Expiry Data : July-05-2018 -> Epoch time : 1531612800 (UTC)
# Expiry Data : August-15-2018 -> Epoch time : 1534291200 (UTC)
# Expiry Data : Dec-15-2018 -> Epoch time : 1544832000 (UTC)
EXP = 1544832000

def check_license():
    if time.time() > EXP:
        sys.exit("License Expired")



nets = {}
# Caffe model Paths

# nets = {"net_gender": net_gender, "net_spec": net_spec, "net_race": net_race, "net_age": net_age}

# age_list = ["0-10", "10-20", "20-50", "20-50", "20-50", "50+"]  ## old
age_list = ["0-9", "10-19", "20-50", "50+"]  # new list
pac_list = ["0-9", "10-19", "20-50", "50+"]  
race_list = ["South-Asian", "East-Asian", "Caucasian", "African"]
gender_list = ["Male", "Female"]
spec_list = ["None", "Specs", "Sunglasses"]

fm_list = ["No-Mask","Mask"]
ubc_list = ["Normal", "Large-Jacket", "Worker-Vest","Suit","Student-Uniform","Not-Visible"]
lbc_list = ["Normal", "Baggy-Pants", "Skirt","Not-Visible"]
hps_list = ["Reaching-Product", "On-Trolley", "Touching-Face", "By-Side"]
hgr_list = ["None", "Cap/Hat/Cycle-Helmet", "Full-Helmet"]

timer = {"n_img": 0, "strt_time": None, "end_time": None}

def person_classifier_accumulator():
    global TIMEOUT

    consumer_receiver = zmq_receiver(5558)
    consumer_sender = zmq_sender(5559)
    # initiialize classifiers connections

    if flags["upper_body_clothing"]:
        ubc_receiver = zmq_receiver(7021)
        uvprint("person_cls_acc. ubc_receiver created")

        # send work
        ubc_sender = zmq_sender(7020)
        uvprint("person_cls_acc. ubc_sender created")

    if flags["lower_body_clothing"]:
        lbc_receiver = zmq_receiver(7031)
        uvprint("person_cls_acc. lbc_receiver created")

        # send work
        lbc_sender = zmq_sender(7030)
        uvprint("person_cls_acc. lbc_sender created")


    if flags["hand_position"]:
        hps_receiver = zmq_receiver(7011)
        uvprint("person_cls_acc. hps_receiver created")

        # send work
        hps_sender = zmq_sender(7010)
        uvprint("person_cls_acc. hps_sender created")


    if flags["person_age"]:
        pac_receiver = zmq_receiver(7041)
        uvprint("person_cls_acc. pac_receiver created")

        # send work
        pac_sender = zmq_sender(7040)
        uvprint("person_cls_acc. pac_sender created")

    
    if flags["person_gender"]:
        pgc_receiver = zmq_receiver(7051)
        uvprint("person_cls_acc. pgc_receiver created")

        # send work
        pgc_sender = zmq_sender(7050)
        uvprint("person_cls_acc. pgc_sender created")



    while True:
        check_license()
        print("person_cls_acc.  waiting for json")
        md = consumer_receiver.recv_json()
        filename = md["data"]["filename"]
        #time_log = md["time_log"]
        print("person_cls_acc. {} json received".format(filename))

        msg = consumer_receiver.recv()
        print("person_cls_acc. {} Image received".format(filename))
        
        md["time_log"]["img_recv_person_cls_acc"] = time.time() # received by person_cls_cc
        # check the image timestamp
        if time.time() - md["time_stamp"] > 10:
            print("classifierC. Image dropped")
            continue

        # Reconstruct Image
        buf = buffer(msg)
        img = np.frombuffer(buf, dtype=md['dtype'])
        img = img.reshape(md['shape'])


        for person in md["data"]["people"] :
            psd_r = [   person["coordinates"]["xmin"],
                        person["coordinates"]["ymin"],
                        person["coordinates"]["xmin"]+person["coordinates"]["width"],
                        person["coordinates"]["ymin"]+person["coordinates"]["height"]
                    ]

            tmp_r = psd_r

            tmp_h = psd_r[3] - psd_r[1]
            tmp_w = psd_r[2] - psd_r[0]

            tmp_r[0] = max(0, int(tmp_r[0] ))
            tmp_r[1] = max(0, int(tmp_r[1] ))
            tmp_r[2] = min(img.shape[1] - 1, int(tmp_r[2] ))
            tmp_r[3] = min(img.shape[0] - 1, int(tmp_r[3] ))

            # for hand position class
            #tmp_r_hps = [0,0,0,0]
            #tmp_r_hps[0] = max(0, int(tmp_r[0] - tmp_w / 5))
            #tmp_r_hps[1] = max(0, int(tmp_r[1] - tmp_h / 5))
            #tmp_r_hps[2] = min(img.shape[1] - 1, int(tmp_r[2] + tmp_w / 5))
            #tmp_r_hps[3] = min(img.shape[0] - 1, int(tmp_r[3] + tmp_h / 5))

            # for ubc, lbc, hps, age/gender classifier
            cropped_person = cv2.resize(img[tmp_r[1]:tmp_r[3] + 1, tmp_r[0]:tmp_r[2] + 1, :], (224, 224))

            person_cls_send_time = time.time()
            md_person = dict(
                dtype=str(cropped_person.dtype),
                shape=cropped_person.shape,
                #data=json_data,
                #imgs_sent=timer['n_img'],
                # time_stamp=md_temp["time_stamp"]
                )            

            # send image to classifiers ##########################################
            if flags["upper_body_clothing"]:
                try:
                    ubc_sender.send_json(md_person)
                    ubc_sender.send(cropped_person, copy=COPY)

                except zmq.error as error:
                    print("person_cls_acc. unable to send to UBC. Error : {}".format(str(error)))
                    continue


            if flags["lower_body_clothing"]:
                try:
                    lbc_sender.send_json(md_person)
                    lbc_sender.send(cropped_person, copy=COPY)

                except zmq.error as error:
                    print("person_cls_acc. unable to send to LBC. Error : {}".format(str(error)))
                    continue


            if flags["hand_position"]:
                try:
                    hps_sender.send_json(md_person)
                    hps_sender.send(cropped_person, copy=COPY)
                    
                except zmq.error as error:
                    print("person_cls_acc. unable to send to HPS. Error : {}".format(str(error)))
                    continue

            if flags["person_age"]:
                try:
                    pac_sender.send_json(md_person)
                    pac_sender.send(cropped_person, copy=COPY)
                    
                except zmq.error as error:
                    print("person_cls_acc. unable to send to PAC. Error : {}".format(str(error)))
                    continue


            if flags["person_gender"]:
                try:
                    pgc_sender.send_json(md_person)
                    pgc_sender.send(cropped_person, copy=COPY)
                    
                except zmq.error as error:
                    print("person_cls_acc. unable to send to PGC. Error : {}".format(str(error)))
                    continue

            # Receive result from classifiers ######################################
            if flags["upper_body_clothing"]:
                try:
                    ubc_result = ubc_receiver.recv_json()
                    uvprint("ubc_result: {}".format(ubc_result))
                    person["properties"]["upper_body_clothing"] = [{"name": ubc_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(ubc_result["ubc_probs"])]                    
                    
                except zmq.error as error:
                    print("UBC taking longer than expected, error : {}".format(str(error)))
                    continue


            if flags["lower_body_clothing"]:
                try:
                    lbc_result = lbc_receiver.recv_json()
                    uvprint("lbc_result: {}".format(lbc_result))
                    person["properties"]["lower_body_clothing"] = [{"name": lbc_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(lbc_result["lbc_probs"])]

                except zmq.error as error:
                    print("LBC taking longer than expected, error : {}".format(str(error)))
                    continue


            if flags["hand_position"]:
                try:
                    hps_result = hps_receiver.recv_json()
                    uvprint("hps_result: {}".format(hps_result))
                    person["properties"]["hand_position"] = [{"name": hps_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(hps_result["hps_probs"])]

                except zmq.error as error:
                    print("HPS taking longer than expected, error : {}".format(str(error)))
                    continue
            
            if flags["person_age"]:
                try:
                    pac_result = pac_receiver.recv_json()
                    uvprint("pac_result: {}".format(pac_result))
                    person["properties"]["person_age"] = [{"name": pac_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(pac_result["pac_probs"])]

                except zmq.error as error:
                    print("HPS taking longer than expected, error : {}".format(str(error)))
                    continue
            
            if flags["person_gender"]:
                try:
                    pgc_result = pgc_receiver.recv_json()
                    uvprint("pgc_result: {}".format(pgc_result))
                    person["properties"]["person_gender"] = [{"name": gender_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(pgc_result["pgc_probs"])]
            
                except zmq.error as error:
                    print("HPS taking longer than expected, error : {}".format(str(error)))
                    continue
            
            
            person_cls_recv_time = time.time()
            md["time_log"]["total_person_classifiers"] = person_cls_recv_time - person_cls_send_time
            md["time_log"]["img_send_person_cls_acc"] = time.time()
            uvprint("time taken by person classifiers : {}".format(md["time_log"]["total_person_classifiers"]))
        try:
            consumer_sender.send_json(md)
        except zmq.error.Again:
            print("Unable to send image dropped")
            continue
            

if __name__ == "__main__":

    check_license()
    try:
        person_classifier_accumulator()
        
    except zmq.error.Again:
        print("classifierC. no packet received in {} ms \n exiting {}...".format(TIMEOUT, sys.argv[0]))
        print("classifierC. {} images sent".format(timer["n_img"]))
        time.sleep(10)
        cv2.destroyAllWindows()
        sys.exit("classifierC. zmq TIMEOUT")
