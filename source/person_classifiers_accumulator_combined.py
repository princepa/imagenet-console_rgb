#!/usr/bin/python
import os
import stat 
import sys
import zmq
import random
import numpy as np
import string
import cv2
import time
import pdb
import _init_paths
from subprocess import Popen, PIPE
import subprocess
# Read configuration flags
from config_parser import flags
from tempfile import mkstemp


TIMEOUT = 0    # timout is ms
HWM = 4
DELAY = 0.0
COPY = False
MODEL_LOAD_DELAY = 4
__DEBUG = False
PERSON_CLASSIFIER_FLAG = flags["person_age"] or flags["person_age"] or flags["lower_body_clothing"] or flags["upper_body_clothing"] or flags["hand_position"]
nets = {}
# Check License Validity
# Expiry Data : July-01-2018 -> Epoch time : 1530403200 (UTC)
# Expiry Data : July-05-2018 -> Epoch time : 1531612800 (UTC)
# Expiry Data : August-15-2018 -> Epoch time : 1534291200 (UTC)
# Expiry Data : Dec-15-2018 -> Epoch time : 1544832000 (UTC)
EXP = 1544832000


if PERSON_CLASSIFIER_FLAG:
    import caffe
    caffe.set_mode_gpu()
    caffe.init_log(1)

def zmq_receiver(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PULL)
    if not TIMEOUT == 0:
        zmq_socket.RCVTIMEO = TIMEOUT           # timeout time to close connection in ms
    zmq_socket.rcvhwm = HWM
    zmq_socket.connect("tcp://127.0.0.1:"+str(port))
    #zmq_socket.bind("tcp://127.0.0.1:5555")
    # Start your result manager and workers before you start your producers
    return zmq_socket


def zmq_sender(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.sndhwm = HWM
    # zmq_socket.SNDTIMEO = TIMEOUT
    zmq_socket.bind("tcp://127.0.0.1:"+str(port))
    # Start your result manager and workers before you start your producers
    return zmq_socket


def uvprint(log):
    if __DEBUG == True:
        print(log)
    else:
        pass


def get_random_name():
    n = random.randint(2,10)
    name = "." + ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(n))
    return name


def get_model_path(model_name):
    model_paths = { "deploy_person_combined":"models/person/person_combined.prototxt.gpg",
                "model_person_combined":"models/person/person_combined.caffemodel.gpg"}
              
    return model_paths[model_name]

def get_popen_command(model_path,p_name):
    cmd = "gpg -d --passphrase uncanny {} > {}".format(model_path,p_name)
    uvprint(cmd)
    return cmd

def check_license():
    if time.time() > EXP:
        sys.exit("License Expired")


def classify(img, net_name, scale_param=0.00390625,channels=1):
    # print(img.shape)
    # print(bl)
    global nets
    net2 = nets[net_name]
    img = img.astype(dtype=float) * scale_param
    shape = net2.blobs['data'].data.shape[2:]
    net2.blobs['data'].reshape(1, channels, shape[0], shape[1])

    
    if channels==1:
        net2.blobs['data'].data[...] = np.transpose(img[:, :, None].astype(dtype=np.float), (2, 0, 1))
    elif channels==3:
        net2.blobs['data'].data[...] = np.transpose(img.astype(dtype=np.float), (2, 0, 1))

    output = net2.forward()
    result = {"age":output['probs_age'][0],"gender":output['probs_gender'][0],"lbc":output['probs_lbc'][0],
            "ubc":output['probs_ubc'][0],"hps":output['probs_handpos'][0]}
    return result


# Caffe model Paths

# nets = {"net_gender": net_gender, "net_spec": net_spec, "net_race": net_race, "net_age": net_age}

# age_list = ["0-10", "10-20", "20-50", "20-50", "20-50", "50+"]  ## old
age_list = ["0-9", "10-19", "20-50", "50+"]  # new list
pac_list = ["0-9", "10-19", "20-50", "50+"]  
race_list = ["South-Asian", "East-Asian", "Caucasian", "African"]
gender_list = ["Male", "Female"]
spec_list = ["None", "Specs", "Sunglasses"]

fm_list = ["No-Mask","Mask"]
ubc_list = ["Normal", "Large-Jacket", "Worker-Vest","Suit","Student-Uniform","Not-Visible"]
lbc_list = ["Normal", "Baggy-Pants", "Skirt","Not-Visible"]
hps_list = ["Reaching-Product", "On-Trolley", "Touching-Face", "By-Side"]
hgr_list = ["None", "Cap/Hat/Cycle-Helmet", "Full-Helmet"]

timer = {"n_img": 0, "strt_time": None, "end_time": None}

def person_classifier_accumulator():
    global TIMEOUT
    
    if PERSON_CLASSIFIER_FLAG:
        deploy_person_combined = get_random_name()
        model_person_combined = get_random_name()
        os.mkfifo(deploy_person_combined)
        _,model_person_combined = mkstemp(model_person_combined)
        cmd = get_popen_command(get_model_path("deploy_person_combined"),deploy_person_combined)
        p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
        # p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
        
        cmd = get_popen_command(get_model_path("model_person_combined"),model_person_combined)
        p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
        time.sleep(MODEL_LOAD_DELAY)
        net_person_combined = caffe.Net(deploy_person_combined, model_person_combined, caffe.TEST)
        os.unlink(deploy_person_combined)
        os.unlink(model_person_combined)
        nets["net_person_combined"] = net_person_combined
        uvprint("done : net_person_combined")

    consumer_receiver = zmq_receiver(5558)
    consumer_sender = zmq_sender(5559)
    # initiialize classifiers connections

    while True:
        check_license()
        print("person_cls_acc.  waiting for json")
        md = consumer_receiver.recv_json()
        filename = md["data"]["filename"]
        #time_log = md["time_log"]
        print("person_cls_acc. {} json received".format(filename))

        msg = consumer_receiver.recv()
        print("person_cls_acc. {} Image received".format(filename))
        
        md["time_log"]["img_recv_person_cls_acc"] = time.time() # received by person_cls_cc
        # check the image timestamp
        if time.time() - md["time_stamp"] > 10:
            print("classifierC. Image dropped")
            continue

        # Reconstruct Image
        buf = buffer(msg)
        img = np.frombuffer(buf, dtype=md['dtype'])
        img = img.reshape(md['shape'])

        person_cls_recv_time = person_cls_semd_time = 0
        for person in md["data"]["people"] :
            psd_r = [   person["coordinates"]["xmin"],
                        person["coordinates"]["ymin"],
                        person["coordinates"]["xmin"]+person["coordinates"]["width"],
                        person["coordinates"]["ymin"]+person["coordinates"]["height"]
                    ]

            tmp_r = psd_r

            tmp_h = psd_r[3] - psd_r[1]
            tmp_w = psd_r[2] - psd_r[0]

            tmp_r[0] = max(0, int(tmp_r[0] ))
            tmp_r[1] = max(0, int(tmp_r[1] ))
            tmp_r[2] = min(img.shape[1] - 1, int(tmp_r[2] ))
            tmp_r[3] = min(img.shape[0] - 1, int(tmp_r[3] ))

            # for hand position class
            #tmp_r_hps = [0,0,0,0]
            #tmp_r_hps[0] = max(0, int(tmp_r[0] - tmp_w / 5))
            #tmp_r_hps[1] = max(0, int(tmp_r[1] - tmp_h / 5))
            #tmp_r_hps[2] = min(img.shape[1] - 1, int(tmp_r[2] + tmp_w / 5))
            #tmp_r_hps[3] = min(img.shape[0] - 1, int(tmp_r[3] + tmp_h / 5))

            # for ubc, lbc, hps, age/gender classifier
            cropped_person = cv2.resize(img[tmp_r[1]:tmp_r[3] + 1, tmp_r[0]:tmp_r[2] + 1, :], (224, 224))

            person_cls_send_time = time.time()
            md_person = dict(
                dtype=str(cropped_person.dtype),
                shape=cropped_person.shape,
                #data=json_data,
                #imgs_sent=timer['n_img'],
                # time_stamp=md_temp["time_stamp"]
                )            

            if PERSON_CLASSIFIER_FLAG:
                t0_person_combined = time.time()
                person_combined_probs = classify(cropped_person, "net_person_combined",channels=3)
                md["time_log"]["person_combined"] = time.time() - t0_person_combined
        
            if flags["upper_body_clothing"]:
                person["properties"]["upper_body_clothing"] = [{"name": ubc_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(person_combined_probs["ubc"])]                    

            if flags["lower_body_clothing"]:
                person["properties"]["lower_body_clothing"] = [{"name": lbc_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(person_combined_probs["lbc"])]

            if flags["hand_position"]:
                person["properties"]["hand_position"] = [{"name": hps_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(person_combined_probs["hps"])]
            
            if flags["person_age"]:
                person["properties"]["person_age"] = [{"name": pac_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(person_combined_probs["age"])]
            
            if flags["person_gender"]:
                person["properties"]["person_gender"] = [{"name": gender_list[indx], "confidence": float(round(conf,4))} for indx, conf in enumerate(person_combined_probs["gender"])]
            
            
        person_cls_recv_time = time.time()
        md["time_log"]["total_person_classifiers"] = person_cls_recv_time - person_cls_send_time
        md["time_log"]["img_send_person_cls_acc"] = time.time()
        uvprint("time taken by person classifiers : {}".format(md["time_log"]["total_person_classifiers"]))
        try:
            consumer_sender.send_json(md)
        except zmq.error.Again:
            print("Unable to send image dropped")
            continue
            

if __name__ == "__main__":

    check_license()
    try:
        person_classifier_accumulator()
        
    except zmq.error.Again:
        print("classifierC. no packet received in {} ms \n exiting {}...".format(TIMEOUT, sys.argv[0]))
        print("classifierC. {} images sent".format(timer["n_img"]))
        time.sleep(10)
        cv2.destroyAllWindows()
        sys.exit("classifierC. zmq TIMEOUT")
