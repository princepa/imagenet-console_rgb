# Code for simple Multi-Layer Perceptron with input data normalised and the keypoints and boxpoints randomly translated within the image to avoid spatial bias
# Input size - 6 X 3
# Hidden Layer 1 - 64
# Hidden Layer 2 - 64
# Output size - 4
import os
import sys
import cv2
import json as JSON 
import numpy as np
import random 
from random import shuffle
import pickle
from config_parser import flags

# Keras imports
from keras import backend as K

import keras
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session,get_session
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten, Reshape
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
from keras.models import load_model


gpu_head_face_det = flags["gpu_head_face_det"]

#######################################################################
#with K.tf.device('/gpu:0'):
with K.tf.device('/gpu:' + str(gpu_head_face_det)):
    config = tf.ConfigProto(intra_op_parallelism_threads=4,\
           inter_op_parallelism_threads=4, allow_soft_placement=True,\
           device_count = {'GPU' : 0})
    config.gpu_options.per_process_gpu_memory_fraction = 0.0005
    session_head = tf.Session(config=config)
    session_head.run(tf.global_variables_initializer())
    K.set_session(session_head)

model_path_head = "models/head_box/head_box.hdf5"
#graph_head = tf.Graph()
#with graph_head.as_default():
#    config = tf.ConfigProto()
#    config.gpu_options.per_process_gpu_memory_fraction = 0.05
#    session_head = tf.Session(config=config)
#    #session_head.run(init_op)
#    with session_head.as_default():
#        session_head.run(tf.global_variables_initializer())
#        model_head = load_model(model_path_head)
#        model_head._make_predict_function() 
#        #session_head.run(tf.global_variables_initializer())    
##os.environ["CUDA_VISIBLE_DEVICES"] = "0"
#config = tf.ConfigProto()
#config.gpu_options.per_process_gpu_memory_fraction = 0.01
#set_session(tf.Session(config=config))
#tf.Session.run(get_session(),tf.global_variables_initializer())

#model_path_head = "models/head_box/head_box.hdf5"
#model  = Sequential()
#input_shape = (6,3)
#model.add(Reshape((1,input_shape[0], input_shape[1]), input_shape=(None,input_shape[0],input_shape[1])))
#model.add(Flatten())
#model.add(Dense(units=64, activation='sigmoid'))
##BatchNormalization()
#model.add(Dense(units=64, activation='sigmoid'))
#BatchNormalization()
#model.add(Dense(units=4,kernel_initializer='normal'))
#model.compile(loss='mean_squared_error', optimizer='adam', metrics=['mse'])

#weights = model.get_weights()
#print("head weights: ",weights)
model_head = load_model(model_path_head)

def get_keypoints_head(keypoints,image_size):
    head_keypoints = [keypoints[1],keypoints[0],keypoints[14],keypoints[15],keypoints[16],keypoints[17]]
    #head_keypoint_copy = head_keypoints[:]
    for points in head_keypoints:
        if points[2] > 0:
            points[2] = 1
        points[0] =  points[0] / image_size[1]
        points[1] =  points[1] / image_size[0]
    return head_keypoints

def get_head_box(keypoints,image_size):
    #model_path_head = "models/head_box/head_box.hdf5"
    # keypoints = 0 nose
    #             14 Reye, 15 L eye, 16 Rear ,17 L ear
    #             1 neck

    # for head box : 0,14,15,16,17 
    #model_head = load_model(model_path_head)
    input_head_keypoints = get_keypoints_head(keypoints,image_size)
    input_head_keypoints = np.array([input_head_keypoints])
    y_pred = model_head.predict(input_head_keypoints, batch_size=1,verbose = 0)
    #y_pred = model_head.predict(input_head_keypoints, batch_size=1,verbose = 0)
    y_pred = y_pred[0]
    y_pred_resized = [y_pred[0]*image_size[1],y_pred[1]*image_size[0],y_pred[2]*image_size[1],y_pred[3]*image_size[0]]
    #print(y_pred_resized)

    # out_shape = [xmin,ymin,xmax,ymax]
    # print("y_pred_resized head box: ",y_pred_resized)
    return y_pred_resized
