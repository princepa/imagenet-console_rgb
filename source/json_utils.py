import json
import os

class JSON_writer():

    def __init__(self, filename):
        self.faces = []
        self.people = []
        self.objects = []
        self.heads = []
        # self.output_path = output_path
        self.filename = filename

    def write_to_json_v4_1(self):
        out_data = {}
        out_data['faces'] = self.faces
        out_data['people'] = self.people
        out_data['objects'] = self.objects
        out_data['heads'] = self.heads
        out_data['filename'] = self.filename

        self.json_data = out_data

        # with open(self.output_path, 'w') as f:
        #     json.dump(out_data, f, indent = 4, ensure_ascii = False)
        #
        # f.close()

    # def addFace(self, x1, y1, x2, y2, attributes, confidence):
    #     assert (type(attributes) == dict) and (type(confidence) == dict), "attributes and keys should be dictionaries "
    #     akeys = attributes.keys()
    #     ckeys = confidence.keys()
    #     akeys.sort()
    #     ckeys.sort()
    #     assert len(akeys) == len(ckeys), "attributes and confidence should have same keys."
    #     box = {}
    #     for k in attributes.keys():
    #         box[k] = attributes[k]
    #     box["coordinates"] = {"xmin": x1, "ymin": y1, "xmax": x2, "ymax": y2}
    #     box['confidence'] = confidence
    #     self.faces.append(box)

    def addPeople(self, x1, y1, x2, y2,conf):
        box = {}
        box["name"] = "People"
        # Old Format
        # box["coordinates"] = {"xmin": x1, "ymin": y1, "xmax": x2, "ymax": y2}
        # New format -> xmin,ymin width,height
        box["coordinates"] = {"xmin": x1, "ymin": y1, "width": x2 - x1, "height": y2 - y1}
        box["confidence"] = conf
        box["properties"] = {}
        self.people.append(box)

    def addHead(self, x1, y1, x2, y2,conf):
        box = {}
        box["name"] = "Head"
        # Old Format
        # box["coordinates"] = {"xmin": x1, "ymin": y1, "xmax": x2, "ymax": y2}
        # New format -> xmin,ymin width,height
        box["coordinates"] = {"xmin": x1, "ymin": y1, "width": x2 - x1, "height": y2 - y1}
        box["confidence"] = conf
        box["properties"] = {}
        self.heads.append(box)

    def addObject(self, x1, y1, x2, y2, name,conf):
        box = {}
        box["name"] = name
        # Old Format
        # box["coordinates"] = {"xmin": x1, "ymin": y1, "xmax": x2, "ymax": y2}
        # New format -> xmin,ymin width,height
        box["coordinates"] = {"xmin": x1, "ymin": y1, "width": x2 - x1, "height": y2 - y1}
        box["confidence"] = conf
        self.objects.append(box)

    # def addShelf(self, x1, y1, x2, y2,conf):
    #     box = {}
    #     box["name"] = "empty-shelf"
    #     # Old Format
    #     # box["coordinates"] = {"xmin": x1, "ymin": y1, "xmax": x2, "ymax": y2}
    #     # New format -> xmin,ymin width,height
    #     box["coordinates"] = {"xmin": x1, "ymin": y1, "width": x2 - x1, "height": y2 - y1}
    #     box["confidence"] = conf
    #     self.objects.append(box)
