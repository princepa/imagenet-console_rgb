#! /usr/bin/python
# COPYRIGHT FUJITSU LIMITED 2018
# AUTHOR : Shubham Jain
# Date : April, 2, 2018


import os
import sys
import zmq
import numpy as np
import time
import cv2
import json
import ast

HWM = 4
COPY = False
DELAY = 0.0
TIMEOUT = 0  # zmq connection timeout in ms

# Expiry Data : July-15-2018 -> Epoch time : 1531612800 (UTC)
# Expiry Data : August-15-2018 -> Epoch time : 1534291200 (UTC)
# Expiry Data : Dec-15-2018 -> Epoch time : 1544832000 (UTC)
EXP = 1544832000

def check_license():
    if time.time() > EXP:
        sys.exit("License Expired")

def zmq_receiver(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PULL)
    #if not TIMEOUT == 0:
    #    zmq_socket.RCVTIMEO = TIMEOUT           # timeout time to close connection in ms
    zmq_socket.rcvhwm = HWM
    zmq_socket.bind("tcp://*:"+str(port))
    #zmq_socket.bind("tcp://127.0.0.1:5555")
    # Start your result manager and workers before you start your producers
    return zmq_socket


def zmq_sender(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.sndhwm = HWM
    # zmq_socket.SNDTIMEO = TIMEOUT
    zmq_socket.bind("tcp://127.0.0.1:"+str(port))
    # Start your result manager and workers before you start your producers
    return zmq_socket



def main():
    check_license()
    recv_port = 5555
    zmq_socket_recv = zmq_receiver(recv_port)

    pose_snd_port = 6555
    demo_snd_port = 5557

    pose_send_socket = zmq_sender(pose_snd_port)
    demo_send_socket = zmq_sender(demo_snd_port)

    
    while True:
        check_license()
        time_log = {}
        print("ImageIn. waiting for json...")

        md_temp = zmq_socket_recv.recv_json()
        try:
            filename = md_temp['filename']
        except KeyError:
            print('KeyError : filename not found in the received json')
            print("message dropped by ImageIn.")
            # to clear the subsequent image in the queue
            msg = zmq_socket_recv.recv(copy=COPY)

            continue

        msg = zmq_socket_recv.recv(copy=COPY)
        print("{} Image received by ImageIn".format(filename))
        time_log["img_recv_imginput"] = time.time()

        try:
            if time.time() - md_temp["time_stamp"] > 10:
                print("image dropped")
                continue
        except KeyError:
            print('KeyError : time_stamp not found in the received json')
            print("message dropped")
            continue

        try:
            dtype = md_temp['dtype']
        except KeyError:
            print('KeyError : dtype not found in the received json')
            print("message dropped")
            continue

        try:
            shape = md_temp['shape']
        except KeyError:
            print('KeyError : shape not found in the received json')
            print("message dropped")
            continue

        time_log["img_send_imginput"] = time.time()
        """        
        # file path 
        file_path = "test_image.jpg" 

        # read the image in memory
        img = cv2.imread(file_path)
        filename = file_path
        """
        # required in the json 
        md = dict(
           # shape and dtype is used by api to reconstruct image  
           dtype=dtype,
           shape=shape,
           filename=filename,
           # time_stamp is used by api to calculate message Timeout
           time_stamp=time.time(),
           time_log=time_log
        )
   
        # Send json and Image to the api
        # send json before image 
        try:
   
            demo_send_socket.send_json(md)
            print("{} json sent to demo".format(filename))
            demo_send_socket.send(msg)
            print("{} Image sent to demo".format(filename))
   
   
            pose_send_socket.send_json(md)
            print("{} json sent to pose".format(filename))
            pose_send_socket.send(msg)
            print("{} Image sent to pose".format(filename))
   
        except zmq.error.Again:
            print("Socket Connection Error")
            sys.exit("unable to send Image in {} ms...check receiver".format(TIMEOUT))
 

if __name__ == "__main__":
    main()
