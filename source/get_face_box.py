# Code for simple Multi-Layer Perceptron with input data normalised and the keypoints and boxpoints randomly translated within the image to avoid spatial bias
# Input size - 6 X 3
# Hidden Layer 1 - 64
# Hidden Layer 2 - 64
# Output size - 4
import os
import sys
import cv2
import json as JSON 
import numpy as np
import random 
from random import shuffle
import pickle
from config_parser import flags
import zmq

# Keras imports

if flags["face"]:
    from keras import backend as K
    import keras
    import tensorflow as tf
    from keras.backend.tensorflow_backend import set_session,get_session
    from keras.models import Sequential 
    from keras.layers import Dense
    from keras.layers import Flatten, Reshape
    from keras.layers.normalization import BatchNormalization
    from keras.optimizers import Adam
    from keras.callbacks import ModelCheckpoint
    from keras.models import load_model

#######################################################################
HWM = 4
COPY = False
TIMEOUT = 0  # zmq connection timeout in ms

gpu_head_face_det = flags["gpu_head_face_det"]

def zmq_receiver(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PULL)
    if not TIMEOUT == 0:
        zmq_socket.RCVTIMEO = TIMEOUT           # timeout time to close connection in ms
    zmq_socket.rcvhwm = HWM
    zmq_socket.connect("tcp://127.0.0.1:"+str(port))
    #zmq_socket.bind("tcp://127.0.0.1:5555")
    # Start your result manager and workers before you start your producers
    return zmq_socket


def zmq_sender(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.sndhwm = HWM
    # zmq_socket.SNDTIMEO = TIMEOUT
    zmq_socket.bind("tcp://127.0.0.1:"+str(port))
    # Start your result manager and workers before you start your producers
    return zmq_socket



model_path_face = "models/face_box/face_box.hdf5"
with K.tf.device('/gpu:' + str(gpu_head_face_det)):
    config = tf.ConfigProto(intra_op_parallelism_threads=4,\
           inter_op_parallelism_threads=4, allow_soft_placement=True,\
           device_count = {'GPU' : 0})
    config.gpu_options.per_process_gpu_memory_fraction = 0.0005
    session_face = tf.Session(config=config)
    session_face.run(tf.global_variables_initializer())
    K.set_session(session_face)
model_face = load_model(model_path_face)

zmq_socket_recv = zmq_receiver(6557)
zmq_socket_send = zmq_sender(6558)


def get_keypoints_face(keypoints,image_size):
    face_keypoints = [keypoints[0],keypoints[14],keypoints[15],keypoints[16],keypoints[17]]
    face_keypoint_copy = face_keypoints[:] 
    for points in face_keypoint_copy:
        if points[2] > 0:
            points[2] = 1
        points[0] =  points[0] / image_size[1]
        points[1] =  points[1] / image_size[0]
    return face_keypoints

def get_face_box(keypoints,image_size):
    #model_face = load_model(model_path_face)
    # keypoints = 0 nose
    #             14 Reye, 15 L eye, 16 Rear ,17 L ear
    #             1 neck

    # for face box : 0,14,15,16,17
    #model_face = load_model(model_path_face)
    input_face_keypoints = get_keypoints_face(keypoints,image_size)
    input_face_keypoints = np.array([input_face_keypoints])
    y_pred = model_face.predict(input_face_keypoints, batch_size=1, verbose = 0)
    #y_pred = model_face.predict(input_face_keypoints, batch_size=1, verbose = 0)
    y_pred = y_pred[0]
    y_pred_resized = [y_pred[0]*image_size[1],y_pred[1]*image_size[0],y_pred[2]*image_size[1],y_pred[3]*image_size[0]]
    #print(y_pred_resized)

    # out_shape = [xmin,ymin,xmax,ymax]
    # print("y_pred_resized face box: ",y_pred_resized)
    return y_pred_resized



if __name__ == '__main__':
    
    if flags["face"]:
        while True:

            print("face_box waiting for msg...")
            md_temp = zmq_socket_recv.recv_json()
            keypoints = md_temp["keypoints"]
            image_size =md_temp["image_size"]
            #print("msg received by face_box: ",md_temp)
            
            face_box = get_face_box(keypoints,image_size)
            result = {"box":face_box}
            zmq_socker_send = zmq_socket_send.send_json(result)
            print("msg sent by face_box")
    else:
        print("face_box exiting...")
        

        
