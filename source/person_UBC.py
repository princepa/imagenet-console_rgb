#!/usr/bin/python
import os
import stat 
import sys
import zmq
import random
import numpy as np
import string
import cv2
import time
import pdb
import _init_paths
from subprocess import Popen, PIPE
import subprocess
# Read configuration flags
from config_parser import flags
from tempfile import mkstemp
import caffe

TIMEOUT = 0    # timout is ms
HWM = 4
DELAY = 0.0
MODEL_LOAD_DELAY = 2
__DEBUG = False

nets = {}

timer = {"n_img": 0, "strt_time": None, "end_time": None}

caffe.set_mode_gpu()
caffe.init_log(1)
gpu_person_class = flags["gpu_person_class"]
caffe.set_device(gpu_person_class)

def uvprint(log):
    if __DEBUG == True:
        print(log)
    else:
        pass

# Check License Validity
# Expiry Data : July-01-2018 -> Epoch time : 1530403200 (UTC)
# Expiry Data : July-05-2018 -> Epoch time : 1531612800 (UTC)
# Expiry Data : August-15-2018 -> Epoch time : 1534291200 (UTC)
# Expiry Data : Dec-15-2018 -> Epoch time : 1544832000 (UTC)
EXP = 1544832000

def check_license():
    if time.time() > EXP:
        sys.exit("License Expired")


def get_random_name():
    n = random.randint(2,10)
    name = "." + ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(n))
    return name


def get_model_path(model_name):
    model_paths = { 
                    "deploy_ubc":"models/upper_body_clothing/ubc.prototxt.gpg",
                    "model_ubc":"models/upper_body_clothing/ubc.caffemodel.gpg"
                  }
    return model_paths[model_name]


def get_popen_command(model_path,p_name):
    cmd = "gpg -d --passphrase uncanny {} > {}".format(model_path,p_name)
    uvprint(cmd)
    return cmd

def zmq_receiver(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PULL)
    if not TIMEOUT == 0:
        zmq_socket.RCVTIMEO = TIMEOUT           # timeout time to close connection in ms
    zmq_socket.rcvhwm = HWM
    zmq_socket.connect("tcp://127.0.0.1:"+str(port))
    #zmq_socket.bind("tcp://127.0.0.1:5555")
    # Start your result manager and workers before you start your producers
    return zmq_socket


def zmq_sender(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.sndhwm = HWM
    # zmq_socket.SNDTIMEO = TIMEOUT
    zmq_socket.bind("tcp://127.0.0.1:"+str(port))
    # Start your result manager and workers before you start your producers
    return zmq_socket



# Check if upper body clothing flag is True (ubc)
if flags["upper_body_clothing"]:
    uvprint("reading UBC model...")
    deploy_ubc = get_random_name()
    model_ubc = get_random_name()
    os.mkfifo(deploy_ubc)
    _,model_ubc = mkstemp(model_ubc)
    cmd = get_popen_command(get_model_path("deploy_ubc"),deploy_ubc)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    cmd = get_popen_command(get_model_path("model_ubc"),model_ubc)
    p = subprocess.Popen(cmd,shell=True,stderr=subprocess.PIPE)
    time.sleep(MODEL_LOAD_DELAY)
    net_ubc = caffe.Net(deploy_ubc, model_ubc, caffe.TEST)
    os.unlink(deploy_ubc)
    os.unlink(model_ubc)
    nets["net_ubc"] = net_ubc
    uvprint("done : ubc")


def classify(img, net_name, scale_param=0.00390625,channels=1):
    global nets
    net2 = nets[net_name]
    img = img.astype(dtype=float) * scale_param
    shape = net2.blobs['data'].data.shape[2:]
    net2.blobs['data'].reshape(1, channels, shape[0], shape[1])

    if channels==1:
        net2.blobs['data'].data[...] = np.transpose(img[:, :, None].astype(dtype=np.float), (2, 0, 1))
    elif channels==3:
        net2.blobs['data'].data[...] = np.transpose(img.astype(dtype=np.float), (2, 0, 1))

    output = net2.forward()
    return output['probs'][0]


def consumer():
    global TIMEOUT
    consumer_receiver = zmq_receiver(7020)
    uvprint("classifierC. receiver created")

    # send work
    consumer_sender =zmq_sender(7021)
    uvprint("classifierC. sender created")

    while True:
        check_license()
        print("UBC.  waiting for json")
        md = consumer_receiver.recv_json()
        print("UBC. json received")

        msg = consumer_receiver.recv()
        print("UBC. Image received")

        # Reconstruct Image
        buf = buffer(msg)
        img = np.frombuffer(buf, dtype=md['dtype'])
        img = img.reshape(md['shape'])
             

        # for ubc classifier
        # cropped_person_ubc_orig = cv2.resize(img[tmp_r[1]:tmp_r[3] + 1, tmp_r[0]:tmp_r[2] + 1, :], (224, 224))
        # mean_ubc = np.array([104,117,123]) # mean in R G B
        # cropped_person_ubc = cropped_person_ubc_orig - mean_ubc

        t0 = time.time()
        ubc_probs = classify(img, "net_ubc",scale_param=1,channels=3)
        t1 = time.time()
        uvprint("time taken by UBC: {}".format(t1-t0))
        ubc_result = {"ubc_probs":ubc_probs.tolist()}
        try:
            consumer_sender.send_json(ubc_result)
        except zmq.error.Again:
            print("Unable to send image dropped")
            continue

        timer["n_img"] += 1



if __name__ == "__main__":

    check_license()
    try:
        if flags["upper_body_clothing"]:
            consumer()

    except zmq.error.Again:
        print("classifierC. no packet received in {} ms \n exiting {}...".format(TIMEOUT, sys.argv[0]))
        print("classifierC. {} images sent".format(timer["n_img"]))
        time.sleep(10)
        cv2.destroyAllWindows()
        sys.exit("classifierC. zmq TIMEOUT")
