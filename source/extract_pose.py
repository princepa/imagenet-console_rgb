import sys
import cv2
import subprocess
import zmq
import os
import time
import numpy as np
#sys.path.append("/app/3rdParty/pose/")

from pose import *
# Read configuration flags
from config_parser import flags

__DEBUG = False
HWM = 4
COPY = False
TIMEOUT = 0  # zmq connection timeout in ms
DELAY = 0.0
MODEL_LOAD_DELAY = 5
gpu_head_face_det = flags["gpu_head_face_det"]

def uvprint(log):
    if __DEBUG == True:
        print(log)
    else:
        pass


def get_popen_command(model_path,p_name):
    cmd = "gpg -d --passphrase uncanny {} > {}".format(model_path,p_name)
    uvprint(cmd)
    return cmd

pose_proto_path = "models/pose/pose.prototxt"
pose_enc_proto_path = "models/pose/pose.prototxt.gpg"

pose_model_path = "models/pose/pose.caffemodel"
pose_enc_model_path = "models/pose/pose.caffemodel.gpg"


if flags["pose"]:
    try :
        os.unlink(pose_proto_path)
        os.unlink(pose_model_path)
    except:
        pass

    os.mkfifo(pose_proto_path)
    cmd = get_popen_command(pose_enc_proto_path,pose_proto_path)
    p = subprocess.Popen(cmd,stderr=subprocess.PIPE,shell=True)
    #p = subprocess.Popen(cmd,stderr=subprocess.PIPE)


    #os.mknod(pose_model_path)
    cmd = get_popen_command(pose_enc_model_path,pose_model_path)
    p = subprocess.Popen(cmd,stderr=subprocess.PIPE,shell=True)
    #p = subprocess.Popen(cmd,stderr=subprocess.PIPE)

    time.sleep(MODEL_LOAD_DELAY)
    
    params = dict()
    params["logging_level"] = 3
    params["output_resolution"] = "-1x-1"
    params["net_resolution"] = "-1x368"
    params["model_pose"] = "COCO"
    params["alpha_pose"] = 0.6
    params["scale_gap"] = 0.3
    params["scale_number"] = 1
    params["render_threshold"] = 0.05
    #params["num_gpu_start"] = 0
    params["num_gpu_start"] = gpu_head_face_det
    params["disable_blending"] = False
    params["default_model_folder"] = "models/pose/" 
    pose = Pose(params)

    time.sleep(MODEL_LOAD_DELAY)
    
    try :
        os.unlink(pose_proto_path)
        os.unlink(pose_model_path)
    except:
        pass


def zmq_receiver(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PULL)
    if not TIMEOUT == 0:
        zmq_socket.RCVTIMEO = TIMEOUT           # timeout time to close connection in ms
    zmq_socket.rcvhwm = HWM
    zmq_socket.connect("tcp://127.0.0.1:"+str(port))
    #zmq_socket.bind("tcp://127.0.0.1:5555")
    # Start your result manager and workers before you start your producers
    return zmq_socket


def zmq_sender(port):
    context = zmq.Context()
    zmq_socket = context.socket(zmq.PUSH)
    zmq_socket.sndhwm = HWM
    # zmq_socket.SNDTIMEO = TIMEOUT
    zmq_socket.bind("tcp://127.0.0.1:"+str(port))
    # Start your result manager and workers before you start your producers
    return zmq_socket



if __name__ == '__main__':
    count = 0 
    zmq_socket_recv = zmq_receiver(6555)
    zmq_socket_send = zmq_sender(6556)


    while True:
        
        print("Pose waiting for json...")
        md_temp = zmq_socket_recv.recv_json()
        try:
            filename = md_temp['filename']
        except KeyError:
            print('KeyError : filename not found in the received json')
            print("message dropped")
            # to clear the subsequent image in the queue
            msg = zmq_socket_recv.recv(copy=COPY)

            continue


        # print("{} received".format(timer["n_img"]))
        print("{} json received by pose".format(filename))
            
        msg = zmq_socket_recv.recv(copy=COPY)
        print("{} Image received by pose".format(filename))

        md_temp["time_log"]["img_recv_pose"] = time.time() 
        try:
            if time.time() - md_temp["time_stamp"] > 10:
                print("image dropped")
                continue
        except KeyError:
            print('KeyError : time_stamp not found in the received json')
            print("message dropped")
            continue
    
        try:
            dtype = md_temp['dtype']
        except KeyError:
            print('KeyError : dtype not found in the received json')
            print("message dropped")
            continue
    
        try:
            shape = md_temp['shape']
        except KeyError:
            print('KeyError : shape not found in the received json')
            print("message dropped")
            continue
    
        count += 1
        # Reconstruct image
        buf = buffer(msg)
        img_matlab = np.frombuffer(buf, dtype=md_temp['dtype'])
        img = img_matlab.reshape(md_temp['shape'])
    
        data = {}
        if flags["pose"]:
            t1 = time.time()
            # poses,output_image = pose.forward(img,True)
            poses = pose.forward(img,False)
            t2 = time.time()
            md_temp["time_log"]["pose_detection"] = t2-t1

            #print("{} images processed in {}s".format(count,t2-t1))    
            #print("output shape: {}".format(poses.shape))
            
            data["poses"] = poses.tolist()

        md_temp["time_log"]["img_send_pose"] = time.time()
        md = dict(
            dtype=str(img.dtype),
            shape=img.shape,
            data=data,
            time_stamp=md_temp["time_stamp"],
            time_log=md_temp["time_log"],
            filename=filename
        )
        #print("json sent by extract pose:",md)
        try:
            zmq_socket_send.send_json(md)
            zmq_socket_send.send(img_matlab, copy=COPY)

        except zmq.error as error:
            print("Demo. Error : {}, {} image dropped by {}".format(str(error),filename,sys.argv[0]))
            continue

