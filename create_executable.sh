# This script will be used to create a intermediate docker image and convert python to executables within this docker image

# Syntax to run the script is shown below:
# ./create_executable.sh tempBuildName

cp dockerignore.executable .dockerignore

echo "Creating temporary build named: $1"
docker build --network=host -f Installerfile -t $1 .

apath=`pwd`
rm -rf temp
echo "Running the temporary build in detached mode"
img=`docker run -itd -v $apath/temp:/app/temp $1`

echo "Deleting the temporary container"
docker wait $img
docker rm -f $img

echo "Deleting folder project-executables"
rm -rf project-executables

echo "Copying new executables to project-executables"
mkdir -p project-executables
cp temp/* project-executables/

